
#include <jni.h>
#include <cassert>

#include "KtMediaPlayerClient.h"
#include "Logger.hpp"

#ifdef ANDROID_NDK_LOG_TAG
#undef ANDROID_NDK_LOG_TAG
#define ANDROID_NDK_LOG_TAG "jni.main"
#endif

JavaVM *g_jvm;

JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
    NDK_ALOGI("[JNI_OnLoad]");

    JNIEnv *env = nullptr;
    g_jvm       = vm;
    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK)
    {
        return JNI_ERR;
    }
    assert(env != nullptr);

    // register java class dai.android.media.service.KtMediaPlayerClient info
    register_KtMediaPlayerClient(env);

    // init global
    ktmedia_global_init();

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNI_OnUnload(JavaVM *vm, void *reserved)
{
    NDK_ALOGI("[JNI_OnUnload]");
    ktmedia_global_uninit();
}



