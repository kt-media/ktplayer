#include "AndroidMediaPlayer.hpp"

NAMESPACE_KT_BEGIN

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// static function
//
void AndroidMediaPlayer::GlobalInitImpl()
{
}

void AndroidMediaPlayer::GlobalUninitImpl()
{
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void AndroidMediaPlayer::startImpl()
{
}

void AndroidMediaPlayer::stopImpl()
{
}

void AndroidMediaPlayer::pauseImpl()
{
}

void AndroidMediaPlayer::releaseImpl()
{
}

void AndroidMediaPlayer::setSurfaceImpl(const void *surface)
{
}

void AndroidMediaPlayer::setDataSourceStrImpl(const char *file)
{
    assert(nullptr != file);
    FastMutex::ScopedLock lock(_mutex);
    if(_ksource.isNull()) _ksource = makeShared<KSource>();
    _ksource->reset();
    _ksource->setCurrentUri(file);

    changePlayState(MP_STATE_INITIALIZED);
}

void AndroidMediaPlayer::setDataSourceKtImpl(void *kt_source)
{
}

void AndroidMediaPlayer::notifyStateChange(PlayState oldValue, PlayState newValue)
{
}


NAMESPACE_KT_END
