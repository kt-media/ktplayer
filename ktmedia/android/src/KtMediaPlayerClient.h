#ifndef INCLUDE_KT_MEDIA_PLAYER_CLIENT_H
#define INCLUDE_KT_MEDIA_PLAYER_CLIENT_H

#include <jni.h>

// global java vm
extern JavaVM *g_jvm;

void ktmedia_global_init();
void ktmedia_global_uninit();

void register_KtMediaPlayerClient(JNIEnv *env);

void  ktmedia_finalize          (JNIEnv *env, jobject thiz);
void  ktmedia_native_setup      (JNIEnv *env, jobject thiz, jobject weak_this, jint type);
void  ktmedia_setDataSourceFd   (JNIEnv *env, jobject thiz, jint fd);
void  ktmedia_setDataSourceStr  (JNIEnv *env, jobject thiz, jstring path);
void  ktmedia_setDataSourceKt   (JNIEnv *env, jobject thiz, jobject source);
void  ktmedia_setSurface        (JNIEnv *env, jobject thiz, jobject surface);
void  ktmedia_prepareAsync      (JNIEnv *env, jobject thiz);
void  ktmedia_start             (JNIEnv *env, jobject thiz);
void  ktmedia_stop              (JNIEnv *env, jobject thiz);
void  ktmedia_release           (JNIEnv *env, jobject thiz);
void  ktmedia_reset             (JNIEnv *env, jobject thiz);
void  ktmedia_pause             (JNIEnv *env, jobject thiz);
void  ktmedia_seekTo            (JNIEnv *env, jobject thiz, jlong msec, jint flag);
jlong ktmedia_getCurrentPosition(JNIEnv *env, jobject thiz);
jlong ktmedia_getDuration       (JNIEnv *env, jobject thiz);
void  ktmedia_setVolume         (JNIEnv *env, jobject thiz, jfloat leftVolume, jfloat rightVolume);


#endif//INCLUDE_KT_MEDIA_PLAYER_CLIENT_H
