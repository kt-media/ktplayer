
#include "j4a/j4a-basic.h"
#include "Logger.hpp"
#include "KtMediaPlayerClient.h"
#include "Mutex.hpp"
#include "MediaPlayer.hpp"

#if defined(KT_OS_LINUX)
#   include <unistd.h>
#endif

#ifdef ANDROID_NDK_LOG_TAG
#undef ANDROID_NDK_LOG_TAG
#define ANDROID_NDK_LOG_TAG "jni.kt-client"
#endif

NAMESPACE_KT_USING

//
// save java class dai.android.media.service.KtMediaPlayerClient basic information
//
typedef struct J4AC_kt_service_KtMediaPlayerClient
{
    jclass     clazz{};
    FastMutex  mutex{};

    struct _field
    {
        jfieldID mNativePlayerClient;
    } field{};

    struct _method
    {
        jmethodID m_xxx;
    } method{};

} _KtMediaPlayerClient;
static _KtMediaPlayerClient g_class{};


static jlong getKtMediaPlayerClient_mNativePlayerClient(JNIEnv *env, jobject thiz)
{
    return env->GetLongField(thiz, g_class.field.mNativePlayerClient);
}
static jlong getCatchKtMediaPlayerClient_mNativePlayerClient(JNIEnv *env, jobject thiz)
{
    jlong ret = getKtMediaPlayerClient_mNativePlayerClient(env, thiz);
    if(J4A_ExceptionCheck__catchAll(env)) return 0;
    return ret;
}

static void setKtMediaPlayerClient_mNativePlayerClient(JNIEnv *env, jobject thiz, jlong value)
{
    env->SetLongField(thiz, g_class.field.mNativePlayerClient, value);
}
static void setCatchKtMediaPlayerClient_mNativePlayerClient(JNIEnv *env, jobject thiz, jlong value)
{
    setKtMediaPlayerClient_mNativePlayerClient(env, thiz, value);
    J4A_ExceptionCheck__catchAll(env);
}

static MediaPlayer *create_android_media_player()
{
    NDK_ALOGI("android create KT native media");
    MediaPlayer *mp = MediaPlayer::CreateMediaPlayer();
    NDK_ALOGI("created android media player install: %p", mp);
    assert(nullptr != mp);
    return mp;
}

static MediaPlayer *jni_get_media_player(JNIEnv *env, jobject thiz)
{
    FastMutex::ScopedLock lock(g_class.mutex);
    jlong pTr = getCatchKtMediaPlayerClient_mNativePlayerClient(env, thiz);
    assert( pTr > 0);
    return reinterpret_cast<MediaPlayer *>(pTr);
}
static MediaPlayer *jni_set_media_player(JNIEnv *env, jobject thiz, MediaPlayer *mp)
{
    FastMutex::ScopedLock lock(g_class.mutex);
    MediaPlayer *oldPlayer = nullptr;
    jlong old = getKtMediaPlayerClient_mNativePlayerClient(env, thiz);
    if(old)
    {
        oldPlayer = reinterpret_cast<MediaPlayer *>(old);
        NDK_ALOGI("old android media player install: %p", oldPlayer);
    }
    setCatchKtMediaPlayerClient_mNativePlayerClient(env, thiz, reinterpret_cast<jlong>(mp));
    return oldPlayer;
}


//--------------------------------------------------------------------------------------------
//
// KtMediaPlayerClient
static const char *KT_MEDIA_PLAYER_CLIENT = "dai/android/media/service/KtMediaPlayerClient";

static JNINativeMethod g_methods[] =
{
    { "native_setup",              "(Ljava/lang/Object;I)V",               (void *)ktmedia_native_setup },
    { "native_finalize",           "()V",                                  (void *)ktmedia_finalize },
    { "native_setDataSourceFd",    "(I)V",                                 (void *)ktmedia_setDataSourceFd },
    { "native_setDataSourceStr",   "(Ljava/lang/String;)V",                (void *)ktmedia_setDataSourceStr },
    { "native_setDataSourceKt",    "(Ldai/android/media/KtDataSource;)V",  (void *)ktmedia_setDataSourceKt },
    { "native_prepareAsync",       "()V",                                  (void *)ktmedia_prepareAsync },
    { "native_start",              "()V",                                  (void *)ktmedia_start },
    { "native_stop",               "()V",                                  (void *)ktmedia_stop },
    { "native_pause",              "()V",                                  (void *)ktmedia_pause },
    { "native_seekTo",             "(JI)V",                                (void *)ktmedia_seekTo },
    { "native_release",            "()V",                                  (void *)ktmedia_release },
    { "native_reset",              "()V",                                  (void *)ktmedia_reset },
    { "native_getDuration",        "()J",                                  (void *)ktmedia_getDuration },
    { "native_getCurrentPosition", "()J",                                  (void *)ktmedia_getCurrentPosition },
    { "native_setVolume",          "(FF)V",                                (void *)ktmedia_setVolume },
    { "native_setVideoSurface",    "(Landroid/view/Surface;)V",            (void *)ktmedia_setSurface },
};


void register_KtMediaPlayerClient(JNIEnv *env)
{
    NDK_ALOGI( "[register_KtMediaPlayerClient]" );

    jclass clazz = env->FindClass(KT_MEDIA_PLAYER_CLIENT);
    if(J4A_ExceptionCheck__catchAll(env) || !clazz)
    {
        NDK_ALOGI("find class failed: %s", KT_MEDIA_PLAYER_CLIENT);
        return;
    }

    g_class.clazz = clazz;
    // register native field
    //
    // mNativePlayerClient
    {
        const char *name  = "mNativePlayerClient";
        const char *sign  = "J";
        jfieldID    value = J4A_GetFieldID__catchAll(env, clazz, name, sign);
        if(!value)
        {
            NDK_ALOGI("find field failed: %s", name);
            return;
        }
        g_class.field.mNativePlayerClient = value;
    }

    /// jobject obj = env->NewGlobalRef(clazz);
    /// if (J4A_ExceptionCheck__catchAll(env) || !(obj))
    /// {
    ///     NDK_ALOGI("new global ref of class class failed: %s", KT_MEDIA_PLAYER_CLIENT);
    ///     env->DeleteLocalRef(clazz);
    ///     return;
    /// }
    /// env->DeleteLocalRef(clazz);

    env->RegisterNatives(clazz, g_methods, sizeof(g_methods) / sizeof(g_methods[0]));
}

void ktmedia_global_init()
{
    MediaPlayer::GlobalInit();
}

void ktmedia_global_uninit()
{
    MediaPlayer::GlobalUninit();
}


//--------------------------------------------------------------------------------------------
// basic media player function
//

#ifndef MP_INSTANCE_CHECK_NO_VAL_RETURN
#define MP_INSTANCE_CHECK_NO_VAL_RETURN( mp ) \
    do{ if(nullptr == (mp)) { \
        NDK_ALOGE( "[%s]: null instance object of 'MediaPlayer'.", __FUNCTION__ ); return; \
    }} while(0)
#endif

#ifndef MP_INSTANCE_CHECK_RETURN_VAL
#define MP_INSTANCE_CHECK_RETURN_VAL(mp, value) \
    do { if(nullptr == (mp)) { \
        NDK_ALOGE( "[%s]: null instance object of 'MediaPlayer'.", __FUNCTION__ ); \
        return (value); \
    }} while(0)
#endif

void ktmedia_native_setup(JNIEnv *env, jobject thiz, jobject weak_this, jint type)
{
    NDK_ALOGI( "[ktmedia_native_setup]" );

    FastMutex::ScopedLock lock(g_class.mutex);
    MediaPlayer *mp  = create_android_media_player();
    jlong  old       = getKtMediaPlayerClient_mNativePlayerClient(env, thiz);
    auto  *oldPlayer = reinterpret_cast<MediaPlayer *>(old);
    if(oldPlayer != nullptr)
    {
        NDK_ALOGI("old android media player install: %p", oldPlayer);
        delete oldPlayer;
    }
    setCatchKtMediaPlayerClient_mNativePlayerClient(env, thiz, reinterpret_cast<jlong>(mp));
}

void ktmedia_finalize(JNIEnv *env, jobject thiz)
{
    NDK_ALOGI( "[ktmedia_finalize]" );
}

void ktmedia_setDataSourceFd(JNIEnv *env, jobject thiz, jint fd)
{
    int dupFd     = dup(fd);
    char uri[128] = { 0x0 };
    snprintf(uri, sizeof(uri), "pipe:%d", dupFd);
    NDK_ALOGI( "[ktmedia_setDataSourceFd]: FD=%d DUPFD=%d URI=%s", fd, dupFd, uri );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);

    mp->setDataSourceStr(uri);
}

void ktmedia_setDataSourceStr(JNIEnv *env, jobject thiz, jstring path)
{
    NDK_ALOGI( "[ktmedia_setDataSourceStr]" );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);

    const char *c_path = env->GetStringUTFChars(path, nullptr);
    NDK_ALOGI( "[ktmedia_setDataSourceStr]: path=%s", c_path );
    mp->setDataSourceStr(c_path);
    env->ReleaseStringUTFChars(path, c_path);
}

void ktmedia_setDataSourceKt(JNIEnv *env, jobject thiz, jobject source)
{
    NDK_ALOGI( "[ktmedia_setDataSourceKt]" );
}

void ktmedia_prepareAsync(JNIEnv *env, jobject thiz)
{
    NDK_ALOGI( "[ktmedia_prepareAsync]" );
}

void ktmedia_start(JNIEnv *env, jobject thiz)
{
    NDK_ALOGI( "[ktmedia_start]" );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);
    mp->start();
}

void ktmedia_stop(JNIEnv *env, jobject thiz)
{
    NDK_ALOGI( "[ktmedia_stop]" );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);
    mp->stop();
}

void ktmedia_release(JNIEnv *env, jobject thiz)
{
    NDK_ALOGI( "[ktmedia_release]" );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);
    mp->release();

    // delete
    delete mp;
}

void ktmedia_reset(JNIEnv *env, jobject thiz)
{
    NDK_ALOGI( "[ktmedia_reset]" );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);
    // mp->();
}

void ktmedia_pause(JNIEnv *env, jobject thiz)
{
    NDK_ALOGI( "[ktmedia_pause]" );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);
    mp->pause();
}

void ktmedia_seekTo(JNIEnv *env, jobject thiz, jlong msec, jint flag)
{
    NDK_ALOGI( "[ktmedia_seekTo]: To=%ld Flag=%d", msec, flag );
    MediaPlayer *mp = jni_get_media_player(env, thiz);
    MP_INSTANCE_CHECK_NO_VAL_RETURN(mp);
    // mp->seek();
}

jlong ktmedia_getCurrentPosition(JNIEnv *env, jobject thiz)
{
    return 0L;
}

jlong ktmedia_getDuration(JNIEnv *env, jobject thiz)
{
    return 0L;
}

void ktmedia_setVolume(JNIEnv *env, jobject thiz, jfloat leftVolume, jfloat rightVolume)
{
    NDK_ALOGI( "[ktmedia_setVolume]: Left=%f Right=%f", leftVolume, rightVolume);
}

void ktmedia_setSurface(JNIEnv *env, jobject thiz, jobject surface)
{
    NDK_ALOGI( "[ktmedia_setSurface]" );
}
