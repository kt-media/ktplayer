#ifndef INCLUDE_ANDROID_MEDIA_PLAYER_H
#define INCLUDE_ANDROID_MEDIA_PLAYER_H

#include "foundation.hpp"
#include "IPlayer.hpp"
#include "Mutex.hpp"

#include <string>

NAMESPACE_KT_BEGIN


class AndroidMediaPlayer: public IPlayerImpl
{
public:
    virtual void startImpl() override;

    virtual void stopImpl() override;

    virtual void pauseImpl() override;

    virtual void releaseImpl() override;

    virtual void setSurfaceImpl(const void *surface) override;

    virtual void setDataSourceStrImpl(const char *file) override;
    virtual void setDataSourceKtImpl(void *kt_source) override;

    virtual ~AndroidMediaPlayer() = default;

public:
    virtual void notifyStateChange(PlayState oldValue, PlayState newValue) override ;

protected:
    FastMutex   _mutex;

protected:
    static void GlobalInitImpl();
    static void GlobalUninitImpl();
};


NAMESPACE_KT_END

#endif//INCLUDE_ANDROID_MEDIA_PLAYER_H
