#ifndef INCLUDE_J4A_BASIC_H
#define INCLUDE_J4A_BASIC_H

#include "Logger.hpp"

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef J4A_UNUSED
#define J4A_UNUSED(x) x __attribute__((unused))
#endif

#ifndef ANDROID_NDK_LOG_TAG
#   define ANDROID_NDK_LOG_TAG "J4A"
#else
#   undef  ANDROID_NDK_LOG_TAG
#   define ANDROID_NDK_LOG_TAG "J4A"
#endif


#define J4A_FUNC_FAIL_TRACE()               do { NDK_ALOGE("%s: failed\n",        __func__);            } while (0)
#define J4A_FUNC_FAIL_TRACE1(x__)           do { NDK_ALOGE("%s: failed: %s\n",    __func__, x__);       } while (0)
#define J4A_FUNC_FAIL_TRACE2(x1__, x2__)    do { NDK_ALOGE("%s: failed: %s %s\n", __func__, x1__, x2__);} while (0)

#define J4A_LOAD_CLASS(class__) \
    do { \
        ret = J4A_loadClass__J4AC_##class__(env); \
        if (ret) goto fail; \
    } while (0)

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Exception Handle
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool J4A_ExceptionCheck__throwAny(JNIEnv *env);

bool J4A_ExceptionCheck__catchAll(JNIEnv *env);

int J4A_ThrowExceptionOfClass(JNIEnv *env, jclass clazz, const char *msg);

int J4A_ThrowException(JNIEnv *env, const char *class_sign, const char *msg);

int J4A_ThrowIllegalStateException(JNIEnv *env, const char *msg);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// References
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
jclass J4A_NewGlobalRef__catchAll(JNIEnv *env, jobject obj);

void J4A_DeleteLocalRef(JNIEnv *env, jobject obj);

void J4A_DeleteLocalRef__p(JNIEnv *env, jobject *obj);

void J4A_DeleteGlobalRef(JNIEnv *env, jobject obj);

void J4A_DeleteGlobalRef__p(JNIEnv *env, jobject *obj);

void J4A_ReleaseStringUTFChars(JNIEnv *env, jstring str, const char *c_str);

void J4A_ReleaseStringUTFChars__p(JNIEnv *env, jstring str, const char **c_str);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Class Load
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int J4A_LoadAll__catchAll(JNIEnv *env);

jclass J4A_FindClass__catchAll(JNIEnv *env, const char *class_sign);

jclass J4A_FindClass__asGlobalRef__catchAll(JNIEnv *env, const char *class_sign);

jmethodID J4A_GetMethodID__catchAll(JNIEnv *env, jclass clazz, const char *method_name,
                                    const char *method_sign);

jmethodID J4A_GetStaticMethodID__catchAll(JNIEnv *env, jclass clazz, const char *method_name,
                                          const char *method_sign);

jfieldID J4A_GetFieldID__catchAll(JNIEnv *env, jclass clazz, const char *field_name,
                                  const char *method_sign);

jfieldID J4A_GetStaticFieldID__catchAll(JNIEnv *env, jclass clazz, const char *field_name,
                                        const char *method_sign);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Misc Functions
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

jbyteArray J4A_NewByteArray__catchAll(JNIEnv *env, jsize capacity);

jbyteArray J4A_NewByteArray__asGlobalRef__catchAll(JNIEnv *env, jsize capacity);
/// int J4A_GetSystemAndroidApiLevel                  (JNIEnv *env);

#ifdef __cplusplus
}
#endif

#endif//INCLUDE_J4A_BASIC_H
