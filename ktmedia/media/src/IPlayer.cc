#include "IPlayer.hpp"
#include "Logger.hpp"

NAMESPACE_KT_BEGIN

IPlayerImpl::~IPlayerImpl()
{
}

IPlayerImpl::IPlayerImpl(): _state{MP_STATE_INVALID_STATE}, _ksource{}
{
}

void IPlayerImpl::changePlayState(PlayState newValue, bool force)
{
    PlayState old_state = _state;
    if(old_state == newValue && !force) return;

    // change the state
    _state = newValue;
    NDK_ALOGI("CHANGE_PLAY_STATE: old[%s] to new[%s].",  get_state_string(old_state), get_state_string(newValue));

    notifyStateChange(old_state, newValue);
}

NAMESPACE_KT_END

