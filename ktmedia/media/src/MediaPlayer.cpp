#include "MediaPlayer.hpp"
#include "Logger.hpp"
#include "PlayState.hpp"

#include <cassert>

NAMESPACE_KT_BEGIN

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// static function
//
void MediaPlayer::GlobalInit()
{
    GlobalInitImpl();
}

void MediaPlayer::GlobalUninit()
{
    GlobalUninitImpl();
}

MediaPlayer *MediaPlayer::CreateMediaPlayer()
{
    return new MediaPlayer();
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MediaPlayer::MediaPlayer()
{
}

MediaPlayer:: ~MediaPlayer()
{
}

void MediaPlayer::start()
{
    startImpl();
}

void MediaPlayer::pause()
{
    pauseImpl();
}

inline void MediaPlayer::stop()
{
    stopImpl();
}

void MediaPlayer::release()
{
    releaseImpl();
}

void MediaPlayer::setSurface(const void *surface)
{
    setSurfaceImpl(surface);
}

void MediaPlayer::setDataSourceStr(const char *file)
{
    assert(file != nullptr);

    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_INITIALIZED);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_ASYNC_PREPARING);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_PREPARED);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_STARTED);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_PAUSED);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_COMPLETED);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_STOPPED);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_ERROR);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_RELEASING);
    MPST_RET_IF_EQ_INT_NO_VAL_RETURN(_state, MP_STATE_END);

    setDataSourceStrImpl(file);
}

void MediaPlayer::setDataSourceKt(void *kt_source)
{
    setDataSourceKtImpl(kt_source);
}

NAMESPACE_KT_END

