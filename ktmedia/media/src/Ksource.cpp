#include "Ksource.hpp"

#include <sstream>

NAMESPACE_KT_BEGIN

KSource::KSource(): _playType{PLAY_TYPE_UNKNOWN}, _backUpUrls{}, _currentUrl{}
{
}

KSource::~KSource()
{
    reset();
}

void KSource::reset()
{
    _playType = PLAY_TYPE_UNKNOWN;
    _currentUrl.clear();
    _backUpUrls.clear();
}

void KSource::setCurrentUri(const std::string& url)
{
    _currentUrl.assign(url);
}

void KSource::addUri(const std::string& url)
{
    _backUpUrls.push_back(url);
}

UInt32 KSource::backUpUrlLength() const
{
    return _backUpUrls.size();
}

const char *KSource::operator[](UInt32 index) const
{
    if(index > _backUpUrls.size()) return nullptr;
    return _backUpUrls[index].c_str();
}

const char *KSource::toCString()
{
    std::ostringstream ksource;
    ksource << "KSource>> _playType=" << _playType << std::endl;
    ksource << " current url=" << _currentUrl << std::endl;
    ksource << " backup urls=" << _backUpUrls.size() << std::endl;
    for (int i = 0; i < _backUpUrls.size(); ++i)
    {
        ksource << "   [" << i << "]: " << _backUpUrls[i] << std::endl;
    }
    return ksource.str().c_str();
}

NAMESPACE_KT_END
