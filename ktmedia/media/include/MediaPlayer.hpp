#ifndef INCLUDE_MEDIAPLAYER_H
#define INCLUDE_MEDIAPLAYER_H

#include <memory>

#include "foundation.hpp"

#if KT_OS == KT_OS_ANDROID
#   include "AndroidMediaPlayer.hpp"
#elif KT_OS == KT_OS_MAC_OS_X
#   include "IosMediaPlayer.hpp"
#endif


NAMESPACE_KT_BEGIN

#if KT_OS == KT_OS_ANDROID
class KT_API_DISABLE MediaPlayer: protected AndroidMediaPlayer, public IPlayer
#elif KT_OS == KT_OS_MAC_OS_X
class KT_API_DISABLE MediaPlayer: protected IosMediaPlayer, public IPlayer
#endif
{
protected:
    MediaPlayer();

public:
    void start() override;

    void pause() override;

    void stop() override;

    void release() override;

    void setSurface(const void *surface) override;

    void setDataSourceStr(const char* file) override;
    void setDataSourceKt(void *kt_source) override;

    virtual ~MediaPlayer();

public:
    static void GlobalInit();
    static void GlobalUninit();

    static MediaPlayer *CreateMediaPlayer();
};


NAMESPACE_KT_END

#endif//INCLUDE_MEDIAPLAYER_H
