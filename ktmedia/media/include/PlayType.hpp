#ifndef INCLUDE_PLAY_TYPE_H
#define INCLUDE_PLAY_TYPE_H

#include "foundation.hpp"

NAMESPACE_KT_BEGIN

enum PlayType
{
    PLAY_TYPE_UNKNOWN = 0,

    // live
    PLAY_TYPE_LIVE    = 100,

    PLAY_TYPE_VOD     = 200,
};

#define MP_PLAY_TYPE_STR(TYPE) #TYPE
static const char *get_play_type_string(int type)
{
    switch(type)
    {
        case PLAY_TYPE_LIVE            :
            return MP_PLAY_TYPE_STR(PLAY_TYPE_LIVE   );
        case PLAY_TYPE_VOD     :
            return MP_PLAY_TYPE_STR(PLAY_TYPE_VOD    );
        default:
            return MP_PLAY_TYPE_STR(PLAY_TYPE_UNKNOWN);
    }
};


NAMESPACE_KT_END


#endif//INCLUDE_PLAY_TYPE_H
