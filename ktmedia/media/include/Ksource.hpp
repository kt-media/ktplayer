#ifndef INCLUDE_DATA_SOURCE_H
#define INCLUDE_DATA_SOURCE_H

#include "foundation.hpp"
#include "PlayType.hpp"
#include "Types.hpp"

#include <string>
#include <vector>

NAMESPACE_KT_BEGIN

const UInt8 MAX_BACK_URL_NUM = 8;

class KSource
{
public:
    KSource();
    virtual ~KSource();

    inline void setPlayType(PlayType type)
    {
        _playType = type;
    }

    inline PlayType getPlayType()
    {
        return _playType;
    }

    void reset();

    void setCurrentUri(const std::string& url);
    inline const std::string& getCurrentUrl() const
    {
        return _currentUrl;
    }

    void addUri(const std::string& url);

    UInt32 backUpUrlLength() const;

    const char *operator[](UInt32 index) const;

    const char *toCString();

private:
    PlayType                 _playType;
    std::string              _currentUrl;
    std::vector<std::string> _backUpUrls;
};

NAMESPACE_KT_END

#endif//INCLUDE_DATA_SOURCE_H
