#ifndef INCLUDE_I_PLAYER_H
#define INCLUDE_I_PLAYER_H

#include "foundation.hpp"
#include "PlayState.hpp"
#include "SharedPtr.hpp"
#include "Ksource.hpp"

#ifdef ANDROID_NDK_LOG_TAG
#undef ANDROID_NDK_LOG_TAG
#define ANDROID_NDK_LOG_TAG "jni.kt-play"
#endif

NAMESPACE_KT_BEGIN

class IPlayer
{
public:
    virtual void start() = 0;

    virtual void stop() = 0;

    virtual void pause() = 0;

    virtual void release() = 0;

    virtual void setSurface(const void *surface) = 0;

    virtual void setDataSourceStr(const char *file) = 0;
    virtual void setDataSourceKt(void *kt_source) = 0;

    virtual ~IPlayer() = default;
};


class IPlayerImpl
{
public:
    virtual void startImpl() = 0;

    virtual void stopImpl() = 0;

    virtual void pauseImpl() = 0;

    virtual void releaseImpl() = 0;

    virtual void setSurfaceImpl(const void *surface) = 0;

    virtual void setDataSourceStrImpl(const char *file) = 0;
    virtual void setDataSourceKtImpl(void *kt_source) = 0;

    virtual ~IPlayerImpl();
    IPlayerImpl();

public:
    PlayState          _state;
    SharedPtr<KSource> _ksource;

    virtual void notifyStateChange(PlayState oldValue, PlayState newValue) = 0;
    void changePlayState(PlayState newValue, bool force = false);
};


NAMESPACE_KT_END

#endif//INCLUDE_I_PLAYER_H
