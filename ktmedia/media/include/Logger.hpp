#ifndef INCLUDE_LOGGER_H
#define INCLUDE_LOGGER_H

#include "foundation.hpp"
#include "Platform.hpp"

//
// For Android
#if   KT_OS == KT_OS_ANDROID
#   include "android-basic-log.hpp"
#   define NDK_VLOGV(...)  __android_log_vprint(ANDROID_LOG_VERBOSE,   ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_VLOGD(...)  __android_log_vprint(ANDROID_LOG_DEBUG,     ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_VLOGI(...)  __android_log_vprint(ANDROID_LOG_INFO,      ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_VLOGW(...)  __android_log_vprint(ANDROID_LOG_WARN,      ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_VLOGE(...)  __android_log_vprint(ANDROID_LOG_ERROR,     ANDROID_NDK_LOG_TAG, __VA_ARGS__)

#   define NDK_ALOGV(...)  __android_log_print(ANDROID_LOG_VERBOSE,    ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_ALOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,      ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_ALOGI(...)  __android_log_print(ANDROID_LOG_INFO,       ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_ALOGW(...)  __android_log_print(ANDROID_LOG_WARN,       ANDROID_NDK_LOG_TAG, __VA_ARGS__)
#   define NDK_ALOGE(...)  __android_log_print(ANDROID_LOG_ERROR,      ANDROID_NDK_LOG_TAG, __VA_ARGS__)

//
// For IOS MAC
#elif KT_OS == KT_OS_MAC_OS_X
#   define NDK_VLOGV(...)
#   define NDK_VLOGD(...)
#   define NDK_VLOGI(...)
#   define NDK_VLOGW(...)
#   define NDK_VLOGE(...)

#   define NDK_ALOGV(...)
#   define NDK_ALOGD(...)
#   define NDK_ALOGI(...)
#   define NDK_ALOGW(...)
#   define NDK_ALOGE(...)

#endif







#endif//INCLUDE_LOGGER_H
