#ifndef INCLUDE_PLAY_STATE_H
#define INCLUDE_PLAY_STATE_H

#include "foundation.hpp"

NAMESPACE_KT_BEGIN

enum PlayState
{
    MP_STATE_INVALID_STATE    = -1,

    /*-
     * setDataSource*()  -> MP_STATE_INITIALIZED
     *
     * reset              -> self
     * release            -> MP_STATE_END
     */
    MP_STATE_IDLE             = 0,

    /*-
     * prepare_async()    -> MP_STATE_ASYNC_PREPARING
     *
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_INITIALIZED      = 1,

    /*-
     *                   ...    -> MP_STATE_PREPARED
     *                   ...    -> MP_STATE_ERROR
     *
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_ASYNC_PREPARING   = 2,

    /*-
     * seek_to()          -> self
     * start()            -> MP_STATE_STARTED
     *
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_PREPARED          = 3,

    /*-
     * seek_to()          -> self
     * start()            -> self
     * pause()            -> MP_STATE_PAUSED
     * stop()             -> MP_STATE_STOPPED
     *                   ...    -> MP_STATE_COMPLETED
     *                   ...    -> MP_STATE_ERROR
     *
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_STARTED           = 4,

    /*-
     * seek_to()          -> self
     * start()            -> MP_STATE_STARTED
     * pause()            -> self
     * stop()             -> MP_STATE_STOPPED
     *
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_PAUSED            = 5,

    /*-
     * seek_to()          -> self
     * start()            -> MP_STATE_STARTED (from beginning)
     * pause()            -> self
     * stop()             -> MP_STATE_STOPPED
     *
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_COMPLETED         = 6,

    /*-
     * stop()             -> self
     * prepare_async()    -> MP_STATE_ASYNC_PREPARING
     *
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_STOPPED           = 7,

    /*-
     * reset              -> MP_STATE_IDLE
     * release            -> MP_STATE_END
     */
    MP_STATE_ERROR             = 8,

    /*-
     * release            -> self
     */
    MP_STATE_RELEASING         = 9,

    /*-
     * release            -> self
     */
    MP_STATE_END               = 10,
};

#define MP_STATE_STR(STATE) #STATE
static const char *get_state_string(int state)
{
    switch(state)
    {
        case MP_STATE_IDLE            :
            return MP_STATE_STR(MP_STATE_IDLE           );
        case MP_STATE_INITIALIZED     :
            return MP_STATE_STR(MP_STATE_INITIALIZED    );
        case MP_STATE_ASYNC_PREPARING :
            return MP_STATE_STR(MP_STATE_ASYNC_PREPARING);
        case MP_STATE_PREPARED        :
            return MP_STATE_STR(MP_STATE_PREPARED       );
        case MP_STATE_STARTED         :
            return MP_STATE_STR(MP_STATE_STARTED        );
        case MP_STATE_PAUSED          :
            return MP_STATE_STR(MP_STATE_PAUSED         );
        case MP_STATE_COMPLETED       :
            return MP_STATE_STR(MP_STATE_COMPLETED      );
        case MP_STATE_STOPPED         :
            return MP_STATE_STR(MP_STATE_STOPPED        );
        case MP_STATE_ERROR           :
            return MP_STATE_STR(MP_STATE_ERROR          );
        case MP_STATE_RELEASING       :
            return MP_STATE_STR(MP_STATE_RELEASING      );
        case MP_STATE_END             :
            return MP_STATE_STR(MP_STATE_END            );
        default:
            return MP_STATE_STR(MP_STATE_INVALID_STATE  );
    }
};


//
// state macro
//
#define MPST_RET_IF_EQ_INT_VAL_RETURN(real, expected, errcode) do { if ((real) == (expected)) return (errcode); } while(0)
#define MPST_RET_IF_EQ_INT_NO_VAL_RETURN(real, expected) do{ if( (real) == (expected) ) return; } while(0)

NAMESPACE_KT_END

#endif//INCLUDE_PLAY_STATE_H
