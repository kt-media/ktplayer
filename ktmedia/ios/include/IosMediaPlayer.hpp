#ifndef INCLUDE_IOS_MEDIA_PLAYER_H
#define INCLUDE_IOS_MEDIA_PLAYER_H

#include "foundation.hpp"
#include "IPlayer.hpp"

NAMESPACE_KT_BEGIN

class IosMediaPlayer: public IPlayerImpl
{
public:
    virtual void startImpl() override;

    virtual void stopImpl() override;

    virtual void pauseImpl() override;

    virtual void releaseImpl() override;

    virtual void setSurfaceImpl(const void *surface) override;

    virtual ~IosMediaPlayer() = default;
};


NAMESPACE_KT_END


#endif//INCLUDE_IOS_MEDIA_PLAYER_H
