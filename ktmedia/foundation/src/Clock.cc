#include "Clock.hpp"
#include "Platform.hpp"
#include "Timestamp.hpp"

#if defined(KT_OS_FAMILY_UNIX)
#   include <ctime>
#   include <unistd.h>
#endif

#include <algorithm>
#undef min
#undef max
#include <limits>

#include <exception>
#include <stdexcept>

NAMESPACE_KT_BEGIN

const Clock::ClockVal Clock::CLOCKVAL_MIN = std::numeric_limits<Clock::ClockVal>::min();
const Clock::ClockVal Clock::CLOCKVAL_MAX = std::numeric_limits<Clock::ClockVal>::max();


Clock::Clock()
{
    update();
}

Clock::Clock(ClockVal tv)
{
    _clock = tv;
}

Clock::Clock(const Clock& other)
{
    _clock = other._clock;
}

Clock& Clock::operator = (const Clock& other)
{
    _clock = other._clock;
    return *this;
}

Clock& Clock::operator = (ClockVal tv)
{
    _clock = tv;
    return *this;
}

void Clock::swap(Clock& timestamp)
{
    std::swap(_clock, timestamp._clock);
}

void Clock::update()
{
#if defined(KT_HAVE_CLOCK_GETTIME)
    struct timespec ts {};
    if (clock_gettime(CLOCK_MONOTONIC, &ts)) throw std::runtime_error("cannot get system clock");
    _clock = ClockVal(ts.tv_sec) * resolution() + ts.tv_nsec / 1000;
#else
    Timestamp now;
    _clock = now.epochMicroseconds();
#endif
}


Clock::ClockDiff Clock::accuracy()
{
#if defined(_POSIX_TIMERS) && defined(_POSIX_MONOTONIC_CLOCK)
    struct timespec ts {};
    if (clock_getres(CLOCK_MONOTONIC, &ts))
        throw std::runtime_error("cannot get system clock");

    ClockVal acc = ClockVal(ts.tv_sec) * resolution() + ts.tv_nsec / 1000;
    return acc > 0 ? acc : 1;
#else
    return 1000;
#endif
}

bool Clock::monotonic()
{
#if defined(_POSIX_TIMERS) && defined(_POSIX_MONOTONIC_CLOCK)
    return true;
#else
    return false;
#endif
}

NAMESPACE_KT_END

