#include "Event.hpp"
#include "Timestamp.hpp"

#include <exception>
#include <stdexcept>
#include <ctime>
#include <sys/time.h>
#include <cerrno>

NAMESPACE_KT_BEGIN

Event::Event(EventType type): _type(type), _state(false)
{
    if(0 != pthread_mutex_init(&_mutex, nullptr)) throw std::runtime_error("cannot create event (mutex)");

#if defined(KT_HAVE_MONOTONIC_PTHREAD_COND_TIMEDWAIT)
    pthread_condattr_t attr;
    if (pthread_condattr_init(&attr))
    {
        pthread_mutex_destroy(&_mutex);
        throw std::runtime_error("cannot create event (condition attribute)");
    }
    if (pthread_condattr_setclock(&attr, CLOCK_MONOTONIC))
    {
        pthread_condattr_destroy(&attr);
        pthread_mutex_destroy(&_mutex);
        throw std::runtime_error("cannot create event (condition attribute clock)");
    }
    if (pthread_cond_init(&_cond, &attr))
    {
        pthread_condattr_destroy(&attr);
        pthread_mutex_destroy(&_mutex);
        throw std::runtime_error("cannot create event (condition)");
    }
    pthread_condattr_destroy(&attr);
#else
    if (pthread_cond_init(&_cond, nullptr))
    {
        pthread_mutex_destroy(&_mutex);
        throw std::runtime_error("cannot create event (condition)");
    }
#endif
}

Event::~Event()
{
    pthread_cond_destroy (&_cond);
    pthread_mutex_destroy(&_mutex);
}

void Event::wait()
{
    if(0 != pthread_mutex_lock(&_mutex)) throw std::runtime_error("wait for event failed (lock)");

    while (!_state)
    {
        if(0 != pthread_cond_wait(&_cond, &_mutex))
        {
            pthread_mutex_unlock(&_mutex);
            throw std::runtime_error("wait for event failed");
        }
    }
    if(_type == EVENT_AUTORESET) _state = false;
    pthread_mutex_unlock(&_mutex);
}

void Event::wait(long milliseconds)
{
    if(!tryWait(milliseconds))
    {
        throw std::runtime_error("timeout");
    }
}

bool Event::tryWait(long milliseconds)
{
    int rc = 0;
    struct timespec abstime {};

#if defined(KT_HAVE_MONOTONIC_PTHREAD_COND_TIMEDWAIT)
    clock_gettime(CLOCK_MONOTONIC, &abstime);
    abstime.tv_sec  +=  milliseconds / 1000;
    abstime.tv_nsec += (milliseconds % 1000) * 1000000;
    if (abstime.tv_nsec >= 1000000000)
    {
        abstime.tv_nsec -= 1000000000;
        abstime.tv_sec++;
    }
#elif defined(KT_HAVE_CLOCK_GETTIME)
    clock_gettime(CLOCK_REALTIME, &abstime);
    abstime.tv_sec  +=  milliseconds / 1000;
    abstime.tv_nsec += (milliseconds % 1000) * 1000000;
    if (abstime.tv_nsec >= 1000000000)
    {
        abstime.tv_nsec -= 1000000000;
        abstime.tv_sec++;
    }
#else
    struct timeval tv {};
    gettimeofday(&tv, nullptr);
    abstime.tv_sec  = tv.tv_sec         +  milliseconds / 1000;
    abstime.tv_nsec = tv.tv_usec * 1000 + (milliseconds % 1000) * 1000000;
    if (abstime.tv_nsec >= 1000000000)
    {
        abstime.tv_nsec -= 1000000000;
        abstime.tv_sec++;
    }
#endif

    if (pthread_mutex_lock(&_mutex) != 0) throw std::runtime_error("wait for event failed (lock)");
    while (!_state)
    {
        if ((rc = pthread_cond_timedwait(&_cond, &_mutex, &abstime)))
        {
            if (rc == ETIMEDOUT) break;
            pthread_mutex_unlock(&_mutex);
            throw std::runtime_error("cannot wait for event");
        }
    }
    if (rc == 0 && _type == EVENT_AUTORESET) _state = false;
    pthread_mutex_unlock(&_mutex);
    return rc == 0;
}

void Event::set()
{
    if (pthread_mutex_lock(&_mutex)) throw std::runtime_error("cannot signal event (lock)");
    _state = true;
    if (pthread_cond_broadcast(&_cond))
    {
        pthread_mutex_unlock(&_mutex);
        throw std::runtime_error("cannot signal event");
    }
    pthread_mutex_unlock(&_mutex);
}

void Event::reset()
{
    if (pthread_mutex_lock(&_mutex)) throw std::runtime_error("cannot reset event");
    _state = false;
    pthread_mutex_unlock(&_mutex);
}

NAMESPACE_KT_END

