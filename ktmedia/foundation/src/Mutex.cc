#include "mutex_inner.h"

NAMESPACE_KT_BEGIN

MutexInner::MutexInner()
{
    pthread_mutexattr_t     attr;
    pthread_mutexattr_init(&attr);
#if defined(PTHREAD_MUTEX_RECURSIVE_NP)
    pthread_mutexattr_settype_np(&attr, PTHREAD_MUTEX_RECURSIVE_NP);
#else
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
#endif
    if (pthread_mutex_init(&_mutex, &attr))
    {
        pthread_mutexattr_destroy(&attr);
        throw std::runtime_error("cannot create mutex");
    }
    pthread_mutexattr_destroy(&attr);
}


MutexInner::MutexInner(bool fast)
{
    pthread_mutexattr_t     attr;
    pthread_mutexattr_init(&attr);
#if defined(PTHREAD_MUTEX_RECURSIVE_NP)
    pthread_mutexattr_settype_np(&attr, fast ? PTHREAD_MUTEX_NORMAL_NP : PTHREAD_MUTEX_RECURSIVE_NP);
#else
    pthread_mutexattr_settype(&attr, fast ? PTHREAD_MUTEX_NORMAL : PTHREAD_MUTEX_RECURSIVE);
#endif
    if (pthread_mutex_init(&_mutex, &attr))
    {
        pthread_mutexattr_destroy(&attr);
        throw std::runtime_error("cannot create mutex");
    }
    pthread_mutexattr_destroy(&attr);
}

MutexInner::~MutexInner()
{
    pthread_mutex_destroy(&_mutex);
}


void MutexInner::lockInner()
{
    if (0 != pthread_mutex_lock(&_mutex))
    {
        throw std::runtime_error("cannot lock mutex");
    }
}

void MutexInner::unlockInner()
{
    if (pthread_mutex_unlock(&_mutex))
    {
        throw std::runtime_error("cannot unlock mutex");
    }
}

bool MutexInner::tryLockInner()
{
    int rc = pthread_mutex_trylock(&_mutex);
    if (rc == 0)
        return true;
    else if (rc == EBUSY)
        return false;
    else
        throw std::runtime_error("cannot lock mutex");
}

bool MutexInner::tryLockInner(long milliseconds)
{
#if defined(KT_HAVE_MUTEX_TIMEOUT)
    struct timespec abstime{};
#if defined(KT_HAVE_CLOCK_GETTIME)
    clock_gettime(CLOCK_REALTIME, &abstime);
    abstime.tv_sec  +=  milliseconds / 1000;
    abstime.tv_nsec += (milliseconds % 1000) * 1000000;
    if (abstime.tv_nsec >= 1000000000)
    {
        abstime.tv_nsec -= 1000000000;
        abstime.tv_sec++;
    }
#else
    struct timeval tv{};
    gettimeofday(&tv, nullptr);
    abstime.tv_sec  = tv.tv_sec         +  milliseconds / 1000;
    abstime.tv_nsec = tv.tv_usec * 1000 + (milliseconds % 1000) * 1000000;
    if (abstime.tv_nsec >= 1000000000)
    {
        abstime.tv_nsec -= 1000000000;
        abstime.tv_sec++;
    }
#endif
    int rc = pthread_mutex_timedlock(&_mutex, &abstime);
    if (rc == 0)
        return true;
    else if (rc == ETIMEDOUT)
        return false;
    else
        throw std::runtime_error("cannot lock mutex");

#else
    const int sleepMillis = 5;
    Timestamp now;
    Timestamp::TimeDiff diff(Timestamp::TimeDiff(milliseconds) * 1000);
    do
    {
        int rc = pthread_mutex_trylock(&_mutex);
        if (rc == 0) return true;
        else if (rc != EBUSY) throw std::runtime_error("cannot lock mutex");

        struct timeval tv {};
        tv.tv_sec  = 0;
        tv.tv_usec = sleepMillis * 1000;
        select(0, nullptr, nullptr, nullptr, &tv);
    }
    while (!now.isElapsed(diff));

    return false;
#endif
}

NAMESPACE_KT_END
