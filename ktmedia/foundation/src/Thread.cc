#include "Thread.hpp"
#include "Timespan.hpp"
#include "Platform.hpp"
#include "Runnable.hpp"

#include <cerrno>
#include <sstream>
#include <utility>

//#include <chrono>
//#include <thread>

NAMESPACE_KT_BEGIN

int Thread::getMinOSPriority(int policy)
{
    return getMinOSPriorityInner(policy);
}

int Thread::getMaxOSPriority(int policy)
{
    return getMaxOSPriorityInner(policy);
}

void Thread::sleep(long milliseconds)
{
    // c++11
    // std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));

    sleepInner(milliseconds);
}

bool Thread::trySleep(long milliseconds)
{
    Thread *pT = Thread::current();
    if(nullptr == pT) return false;
    return !(pT->_event.tryWait(milliseconds));
}

Thread *Thread::current()
{
    return static_cast<Thread *>(currentInner());
}

void Thread::yield()
{
    yieldInner();
}

AtomicCounter::ValueType Thread::uniqueId()
{
    static AtomicCounter counter;
    return ++counter;
}
//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------
// inner class RunnableHolder
//--------------------------------------------------------------------------------------------------
namespace {
class RunnableHolder: public Runnable
{
public:
    explicit RunnableHolder(Runnable& target): _target(target)
    {
    }
    ~RunnableHolder() override = default;
    void run() override
    {
        _target.run();
    }
private:
    Runnable& _target;
};

class CallableHolder: public Runnable
{
public:
    CallableHolder(Thread::Callable callable, void *pData): _callable(callable), _pData(pData)
    {
    }
    ~CallableHolder() override = default;
    void run() override
    {
        _callable(_pData);
    }

private:
    Thread::Callable  _callable;
    void             *_pData;
};
}// namespace


//--------------------------------------------------------------------------------------------------
//
std::string Thread::makeName()
{
    std::ostringstream name;
    name << '#' << _id;
    return name.str();
}

Thread::Thread(): _id(uniqueId()), _name(makeName()), _event()
{
}

Thread::Thread(const std::string& name): _id(uniqueId()), _name(name), _event()
{
}

void Thread::setName(const char *name)
{
    FastMutex::ScopedLock lock(_mutex);
    _name.assign(name);
}

std::string Thread::getName()
{
    FastMutex::ScopedLock lock(_mutex);
    return _name;
}

Priority Thread::getPriority() const
{
    return Priority(getPriorityInner());
}

void Thread::setPriority(Priority prio)
{
    setPriorityInner(prio);
}

void Thread::setOSPriority(int prio, int policy)
{
    setOSPriorityInner(prio, policy);
}

int Thread::getOSPriority() const
{
    return getOSPriorityInner();
}

void Thread::wakeUp()
{
    _event.set();
}

void Thread::setStackSize(int size)
{
    setStackSizeInner(size);
}

int Thread::getStackSize() const
{
    return getStackSizeInner();
}

void Thread::join()
{
    joinInner();
}

void Thread::join(long milliseconds)
{
    if (!joinInner(milliseconds)) throw std::runtime_error("runtime timeout");
}

bool Thread::tryJoin(long milliseconds)
{
    return joinInner(milliseconds);
}

void Thread::start(Runnable& target)
{
    startInner(new RunnableHolder(target));
}

void Thread::start(Callable target, void *pData)
{
    startInner(new CallableHolder(target, pData));
}

void Thread::start(SharedPtr<Runnable> target)
{
    startInner(std::move(target));
}

int Thread::id() const
{
    return _id;
}

Thread::TID Thread::tid() const
{
    return currentTidInner();
}

NAMESPACE_KT_END
