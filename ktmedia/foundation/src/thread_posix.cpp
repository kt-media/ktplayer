#include "thread_posix.h"
#include "Timespan.hpp"
#include "Platform.hpp"

#if KT_OS == KT_OS_LINUX || KT_OS == KT_OS_ANDROID || KT_OS == KT_OS_MAC_OS_X
#   include <ctime>
#endif
#include <csignal>
#include <utility>

NAMESPACE_KT_BEGIN

ThreadInner::CurrentThreadHolder ThreadInner::_currentThreadHolder;

ThreadInner::ThreadInner(): _pData(new ThreadData)
{
}

ThreadInner::~ThreadInner()
{
    if(_pData->started && !_pData->joined)
    {
        pthread_detach(_pData->thread);
    }
}

void ThreadInner::setPriorityInner(int prio)
{
    if(prio != _pData->prio)
    {
        _pData->prio   = prio;
        _pData->policy = SCHED_OTHER;
        if(isRunningInner())
        {
            struct sched_param par {};
            //struct MyStruct {};
            par.sched_priority = mapPrio(_pData->prio, SCHED_OTHER);
            if (pthread_setschedparam(_pData->thread, SCHED_OTHER, &par))
                throw std::runtime_error("cannot set thread priority");
        }
    }
}

void ThreadInner::setOSPriorityInner(int prio, int policy)
{
    if(prio != _pData->osPrio || policy != _pData->policy)
    {
        if(_pData->runnableTarget)
        {
            struct sched_param par {};
            par.sched_priority = prio;
            if (pthread_setschedparam(_pData->thread, policy, &par))
                throw std::runtime_error("cannot set thread priority");
        }
        _pData->prio   = reverseMapPrio(prio, policy);
        _pData->osPrio = prio;
        _pData->policy = policy;
    }
}

void ThreadInner::setStackSizeInner(int size)
{
#ifndef PTHREAD_STACK_MIN
    _pData->stack_size = 0;
#else
    if (size != 0)
    {
#if defined(KT_OS_FAMILY_BSD)
        // we must round up to a multiple of the memory page size
        const int STACK_PAGE_SIZE = 4096;
        size = ((size + STACK_PAGE_SIZE - 1) / STACK_PAGE_SIZE) * STACK_PAGE_SIZE;
#endif
        if (size < PTHREAD_STACK_MIN) size = PTHREAD_STACK_MIN;
    }
    _pData->stack_size = size;
#endif
}

void ThreadInner::startInner(SharedPtr<Runnable> pTarget)
{
    if(_pData->runnableTarget != nullptr)
    {
        throw std::runtime_error("thread already running");
    }

    pthread_attr_t     attributes;
    pthread_attr_init(&attributes);
    if (_pData->stack_size != 0)
    {
        if (0 != pthread_attr_setstacksize(&attributes, _pData->stack_size))
        {
            pthread_attr_destroy(&attributes);
            throw std::runtime_error("cannot set thread stack size");
        }
    }

    _pData->runnableTarget = std::move(pTarget);
    if (pthread_create(&_pData->thread, &attributes, runnableEntry, this))
    {
        _pData->runnableTarget = nullptr;
        pthread_attr_destroy(&attributes);
        throw std::runtime_error("cannot start thread");
    }
    _pData->started = true;
    pthread_attr_destroy(&attributes);

    if (_pData->policy == SCHED_OTHER)
    {
        if (_pData->prio != THREAD_PRIO_NORMAL)
        {
            struct sched_param par {};
            par.sched_priority = mapPrio(_pData->prio, SCHED_OTHER);
            if (pthread_setschedparam(_pData->thread, SCHED_OTHER, &par))
                throw std::runtime_error("cannot set thread priority");
        }
    }
    else
    {
        struct sched_param par {};
        par.sched_priority = _pData->osPrio;
        if (pthread_setschedparam(_pData->thread, _pData->policy, &par))
            throw std::runtime_error("cannot set thread priority");
    }
}

void ThreadInner::joinInner()
{
    if(!_pData->started) return;
    _pData->done.wait();
    void *result;
    if (pthread_join(_pData->thread, &result))
        throw std::runtime_error("cannot join thread");
    _pData->joined = true;
}

bool ThreadInner::joinInner(long milliseconds)
{
    if (_pData->started && _pData->done.tryWait(milliseconds))
    {
        void *result;
        if (pthread_join(_pData->thread, &result))
            throw std::runtime_error("cannot join thread");
        _pData->joined = true;
        return true;
    }
    else
        return !_pData->started;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// static function
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void *ThreadInner::runnableEntry (void *pThread)
{
    _currentThreadHolder.set(reinterpret_cast<ThreadInner *>(pThread));
#if defined(KT_OS_FAMILY_UNIX)
    sigset_t     sset;
    sigemptyset(&sset);
    sigaddset  (&sset, SIGQUIT);
    sigaddset  (&sset, SIGTERM);
    sigaddset  (&sset, SIGPIPE);
    pthread_sigmask(SIG_BLOCK, &sset, 0);
#endif
    auto                *pUseThread = reinterpret_cast<ThreadInner *>(pThread);
    AutoPtr<ThreadData>  pData      = pUseThread->_pData;
    try
    {
        pData->runnableTarget->run();
    }
    catch (...)
    {
        throw std::runtime_error("thread runtime error");
    }
    pData->runnableTarget = nullptr;
    pData->done.set();
    return nullptr;
}

int ThreadInner::mapPrio(int prio, int policy)
{
    int pmin = getMinOSPriorityInner(policy);
    int pmax = getMaxOSPriorityInner(policy);
    switch(prio)
    {
        case THREAD_PRIO_LOWEST:
            return pmin;
        case THREAD_PRIO_LOW:
            return pmin +     ( pmax - pmin) / 4;
        case THREAD_PRIO_NORMAL:
            return pmin +     ( pmax - pmin) / 2;
        case THREAD_PRIO_HIGH:
            return pmin + 3 * ( pmax - pmin) / 4;
        case THREAD_PRIO_HIGHEST:
            return pmax;
        default:
            throw std::runtime_error("invalid thread priority");
    }
    return -1;
}

int ThreadInner::reverseMapPrio(int prio, int policy)
{
    if (policy == SCHED_OTHER)
    {
        int pmin   = getMinOSPriorityInner(policy);
        int pmax   = getMaxOSPriorityInner(policy);
        int normal = pmin + (pmax - pmin) / 2;
        if (prio == pmax)  return THREAD_PRIO_HIGHEST;
        if (prio > normal)
            return THREAD_PRIO_HIGH;
        else if (prio == normal)
            return THREAD_PRIO_NORMAL;
        else if (prio > pmin)
            return THREAD_PRIO_LOW;
        else
            return THREAD_PRIO_LOWEST;
    }
    else
        return THREAD_PRIO_HIGHEST;
}

int ThreadInner::getMinOSPriorityInner(int policy)
{
    return sched_get_priority_min(policy);
}

int ThreadInner::getMaxOSPriorityInner(int policy)
{
    return sched_get_priority_max(policy);
}

void ThreadInner::sleepInner(long milliseconds)
{
    //
    // very simple for c++11 or higher
    // std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
    //

#if KT_OS == KT_OS_LINUX || KT_OS == KT_OS_ANDROID || KT_OS == KT_OS_MAC_OS_X
    Timespan remainingTime(1000 * Timespan::TimeDiff(milliseconds));
    int rc;
    do
    {
        struct timespec ts {};
        ts.tv_sec  = (long) remainingTime.totalSeconds();
        ts.tv_nsec = (long) remainingTime.useconds() * 1000;
        Timestamp start;
        rc = ::nanosleep(&ts, nullptr);
        if (rc < 0 && errno == EINTR)
        {
            Timestamp end;
            Timespan waited = start.elapsed();
            if (waited < remainingTime)
                remainingTime -= waited;
            else
                remainingTime = 0;
        }
    }
    while (remainingTime > 0 && rc < 0 && errno == EINTR);
    if (rc < 0 && remainingTime > 0) throw std::runtime_error("nanosleep() failed");
#else
    Timespan remainingTime(1000 * Timespan::TimeDiff(milliseconds));
    int rc;
    do
    {
        struct timeval tv {};
        tv.tv_sec  = (long) remainingTime.totalSeconds();
        tv.tv_usec = (long) remainingTime.useconds();
        Timestamp start;
        rc = ::select(0, nullptr, nullptr, nullptr, &tv);
        if (rc < 0 && errno == EINTR)
        {
            Timestamp end;
            Timespan waited = start.elapsed();
            if (waited < remainingTime)
                remainingTime -= waited;
            else
                remainingTime = 0;
        }
    }
    while (remainingTime > 0 && rc < 0 && errno == EINTR);
    if (rc < 0 && remainingTime > 0) throw std::runtime_error("select() failed");
#endif
}

ThreadInner *ThreadInner::currentInner()
{
    return _currentThreadHolder.get();
}

ThreadInner::TID_INNER ThreadInner::currentTidInner()
{
    return pthread_self();
}

NAMESPACE_KT_END
