#ifndef INCLUDE_FOUNDATION_H
#define INCLUDE_FOUNDATION_H

//
// KT_DEPRECATED
//
// A macro expanding to a compiler-specific clause to
// mark a class or function as deprecated.
//
#if defined(_GNUC_)
#   define KT_DEPRECATED __attribute__((deprecated))
#elif defined(__clang__)
#   define KT_DEPRECATED __attribute__((deprecated))
#elif defined(_MSC_VER)
#   define KT_DEPRECATED __declspec(deprecated)
#else
#   define KT_DEPRECATED
#endif


//
// Ensure that KT_DLL is default unless KT_STATIC is defined
//
#if defined(_WIN32) && defined(_DLL)
#   if !defined(KT_DLL) && !defined(KT_STATIC)
#       define KT_DLL
#   endif
#endif

#if (defined(_WIN32) || defined(_WIN32_WCE)) && defined(KT_DLL)
#   if defined(KT_EXPORTS)
#       define KT_API __declspec(dllexport)
#   else
#       define KT_API __declspec(dllimport)
#   endif
#endif

#ifndef KT_API
#   define KT_API __attribute__ ((visibility ("default")))
#endif

#ifndef KT_API_DISABLE
#   define KT_API_DISABLE __attribute__ ((visibility ("hidden")))
#endif



#ifndef NAMESPACE_KT_BEGIN
#define NAMESPACE_KT_BEGIN namespace KT {
#endif

#ifndef NAMESPACE_KT_END
#define NAMESPACE_KT_END                }
#endif

#ifndef NAMESPACE_KT_USING
#define NAMESPACE_KT_USING using namespace KT;
#endif

#endif//INCLUDE_FOUNDATION_H
