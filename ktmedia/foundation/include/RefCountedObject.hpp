#ifndef INCLUDE_REF_COUNTER_OBJECT_H
#define INCLUDE_REF_COUNTER_OBJECT_H

#include "foundation.hpp"
#include "AtomicCounter.hpp"

NAMESPACE_KT_BEGIN

/// A base class for objects that employ
/// reference counting based garbage collection.
///
/// Reference-counted objects inhibit construction
/// by copying and assignment.
class KT_API RefCountedObject
{
public:
    /// Creates the RefCountedObject.
    /// The initial reference count is one.
    RefCountedObject(): _counter(1) {}

    /// Increments the object's reference count.
    void duplicate() const;

    /// Decrements the object's reference count
    /// and deletes the object if the count
    /// reaches zero.
    void release() const noexcept;

    /// Returns the reference count.
    int referenceCount() const;

protected:
    /// Destroys the RefCountedObject.
    virtual ~RefCountedObject() {}


private:
    RefCountedObject(const RefCountedObject&);
    RefCountedObject& operator = (const RefCountedObject&);

    mutable AtomicCounter _counter;
};


//
// inlines
//
inline int RefCountedObject::referenceCount() const
{
    return _counter.value();
}

inline void RefCountedObject::duplicate() const
{
    ++_counter;
}

inline void RefCountedObject::release() const noexcept
{
    try
    {
        if (--_counter == 0) delete this;
    }
    catch (...)
    {
    }
}

NAMESPACE_KT_END

#endif//INCLUDE_REF_COUNTER_OBJECT_H
