#ifndef INCLUDE_SCOPED_UNLOCK_H
#define INCLUDE_SCOPED_UNLOCK_H

#include "foundation.hpp"

NAMESPACE_KT_BEGIN

/// A class that simplifies thread synchronization
/// with a mutex.
/// The constructor accepts a Mutex and unlocks it.
/// The destructor locks the mutex.
template <class M>
class KT_API ScopedUnlock
{
public:
    inline ScopedUnlock(M& mutex, bool unlockNow = true): _mutex(mutex)
    {
        if (unlockNow) _mutex.unlock();
    }

    inline ~ScopedUnlock()
    {
        try
        {
            _mutex.lock();
        }
        catch (...)
        {
        }
    }

private:
    M& _mutex;

    ScopedUnlock();
    ScopedUnlock(const ScopedUnlock&);
    ScopedUnlock& operator = (const ScopedUnlock&);
};

NAMESPACE_KT_END

#endif//INCLUDE_SCOPED_UNLOCK_H
