#ifndef INCLUDE_THREAD_POSIX_H
#define INCLUDE_THREAD_POSIX_H

#include "foundation.hpp"
#include "Runnable.hpp"
#include "Event.hpp"
#include "Types.hpp"
#include "Mutex.hpp"
#include "AtomicCounter.hpp"
#include "RefCountedObject.hpp"
#include "AutoPtr.hpp"
#include "SharedPtr.hpp"
#include "Platform.hpp"

#include <pthread.h>
#include <string>
#include <exception>
#include <stdexcept>
#include <memory>

NAMESPACE_KT_BEGIN

/**
 *  thread priority
 */
enum Priority
{
    THREAD_PRIO_LOWEST,
    THREAD_PRIO_LOW,
    THREAD_PRIO_NORMAL,
    THREAD_PRIO_HIGH,
    THREAD_PRIO_HIGHEST
};

/**
 * thread policy
 */
enum Policy
{
    THREAD_POLICY_DEFAULT = SCHED_OTHER
};


template <class Functor>
class FunctorRunnable: public Runnable
{
public:
    FunctorRunnable(const Functor& functor): _functor(functor)
    {
    }

    FunctorRunnable(Functor&& functor): _functor(std::move(functor))
    {
    }

    ~FunctorRunnable()
    {
    }

    void run()
    {
        _functor();
    }

private:
    Functor _functor;
};


//
// A inner class for basic of Thread
//
class KT_API ThreadInner
{
public:
    using TID_INNER = pthread_t;
    using Callable  = void(*)(void *);

private:
    struct CurrentThreadHolder
    {
        CurrentThreadHolder()
        {
            if (pthread_key_create(&_key, NULL))
            {
                throw std::runtime_error("cannot allocate thread context key");
            }
        }
        ~CurrentThreadHolder()
        {
            pthread_key_delete(_key);
        }
        ThreadInner *get() const
        {
            return reinterpret_cast<ThreadInner *>(pthread_getspecific(_key));
        }
        void set(ThreadInner *pThread)
        {
            pthread_setspecific(_key, pThread);
        }
    private:
        pthread_key_t _key;
    };

    struct ThreadData: public RefCountedObject
    {
        SharedPtr<Runnable> runnableTarget;
        pthread_t           thread;
        int                 prio;
        int                 osPrio;
        int                 policy;
        Event               done;
        UInt32              stack_size;
        bool                started;
        bool                joined;

        ThreadData():
            thread(0), prio(THREAD_PRIO_NORMAL), osPrio{},       policy{SCHED_OTHER},
            done{},    stack_size(0),            started{false}, joined{false}
        {
        }
    };

private:
    static CurrentThreadHolder _currentThreadHolder;

protected:
    static void *runnableEntry (void *pThread);
    static int   mapPrio       (int   prio,   int policy = SCHED_OTHER);
    static int   reverseMapPrio(int   osPrio, int policy = SCHED_OTHER);

private:
    AutoPtr<ThreadData> _pData;

public:
    static int getMinOSPriorityInner(int policy);
    static int getMaxOSPriorityInner(int policy);

    static void sleepInner(long milliseconds);
    static void yieldInner();
    static ThreadInner *currentInner();
    static TID_INNER    currentTidInner();

public:
    ThreadInner();
    ~ThreadInner();

    TID_INNER tidInner() const;

    void setPriorityInner(int prio);
    int  getPriorityInner() const;

    void setOSPriorityInner(int prio, int policy = SCHED_OTHER);
    int  getOSPriorityInner() const;

    void setStackSizeInner(int size);
    int  getStackSizeInner() const;

    void startInner(SharedPtr<Runnable> pTarget);

    void joinInner();
    bool joinInner(long milliseconds);

    bool isRunningInner() const;
};

//-------------------------------------------------------------------------------
// inlines
//-------------------------------------------------------------------------------

inline int ThreadInner::getPriorityInner() const
{
    return _pData->policy;
}

inline int ThreadInner::getOSPriorityInner() const
{
    return _pData->osPrio;
}

inline bool ThreadInner::isRunningInner() const
{
    return !_pData->runnableTarget.isNull();
}

inline void ThreadInner::yieldInner()
{
    sched_yield();
}

inline int ThreadInner::getStackSizeInner() const
{
    return static_cast<int>(_pData->stack_size);
}

inline ThreadInner::TID_INNER ThreadInner::tidInner() const
{
    return _pData->thread;
}

NAMESPACE_KT_END

#endif//INCLUDE_THREAD_POSIX_H
