#ifndef INCLUDE_RUNNABLE_H
#define INCLUDE_RUNNABLE_H

#include "foundation.hpp"

NAMESPACE_KT_BEGIN

/// The Runnable interface with the run() method
/// must be implemented by classes that provide
/// an entry point for a thread.
class KT_API Runnable
{
public:
    Runnable()          {};
    virtual ~Runnable() {};

    /// Do whatever the thread needs to do. Must
    /// be overridden by subclasses.
    virtual void run() = 0;
};

NAMESPACE_KT_END

#endif//INCLUDE_RUNNABLE_H
