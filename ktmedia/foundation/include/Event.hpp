#ifndef INCLUDE_EVENT_H
#define INCLUDE_EVENT_H

#include "foundation.hpp"
#include <pthread.h>

NAMESPACE_KT_BEGIN

/**
 * An Event is a synchronization object that
 * allows one thread to signal one or more
 * other threads that a certain event has happened.
 * Usually, one thread signals an event,
 * while one or more other threads wait
 * for an event to become signalled.
 */
class KT_API Event
{
public:
    enum EventType
    {
        EVENT_MANUALRESET, /// Manual reset event
        EVENT_AUTORESET    /// Auto-reset event
    };


    /// Creates the event. If type is EVENT_AUTORESET,
    /// the event is automatically reset after
    /// a wait() successfully returns.
    explicit Event(EventType type = EVENT_AUTORESET);

    ~Event();

    /// Signals the event. If is EVENT_AUTORESET,
    /// only one thread waiting for the event can resume execution.
    /// If EVENT_AUTORESET is not, all waiting threads can resume execution.
    void set();

    /// Waits for the event to become signalled.
    void wait();

    /// Waits for the event to become signalled.
    /// Throws a TimeoutException if the event
    /// does not become signalled within the specified
    /// time interval.
    void wait(long milliseconds);

    /// Waits for the event to become signalled.
    /// Returns true if the event
    /// became signalled within the specified
    /// time interval, false otherwise.
    bool tryWait(long milliseconds);


    /// Resets the event to unsignalled state.
    void reset();

private:
    EventType       _type;
    volatile bool   _state;
    pthread_mutex_t _mutex{};
    pthread_cond_t  _cond{};

private:
    Event(const Event&)              = default;
    Event& operator = (const Event&) = default;
};

NAMESPACE_KT_END

#endif//INCLUDE_EVENT_H
