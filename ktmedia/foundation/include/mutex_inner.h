#ifndef INCLUDE_MUTEX_INNER_H
#define INCLUDE_MUTEX_INNER_H

#include "foundation.hpp"
#include "ScopedLock.hpp"
#include "Timespan.hpp"
#include "Platform.hpp"

#include <pthread.h>
#include <cerrno>
#include <exception>
#include <stdexcept>

NAMESPACE_KT_BEGIN

class KT_API MutexInner
{
public:
    /// creates the Mutex.
    MutexInner();

    /// destroys the Mutex.
    ~MutexInner();

    /// Locks the mutex. Blocks if the mutex
    /// is held by another thread.
    void lockInner();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread. Throws a TimeoutException
    /// if the mutex can not be locked within the given timeout.
    ///
    /// Performance Note: On most platforms (including Windows), this member function is
    /// implemented using a loop calling (the equivalent of) tryLock() and Thread::sleep().
    /// On POSIX platforms that support pthread_mutex_timedlock(), this is used.
    void lockInner(long milliseconds);

    /// Unlocks the mutex so that it can be acquired by
    /// other threads.
    void unlockInner();

    /// Tries to lock the mutex. Returns false immediately
    /// if the mutex is already held by another thread.
    /// Returns true if the mutex was successfully locked.
    bool tryLockInner();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread.
    /// Returns true if the mutex was successfully locked.
    ///
    /// Performance Note: On most platforms (including Windows), this member function is
    /// implemented using a loop calling (the equivalent of) tryLock() and Thread::sleep().
    /// On POSIX platforms that support pthread_mutex_timedlock(), this is used.
    bool tryLockInner(long milliseconds);

protected:
    MutexInner(bool fast);

private:
    pthread_mutex_t _mutex;

private:
    MutexInner(const MutexInner&);
    MutexInner& operator = (const MutexInner&);
};

NAMESPACE_KT_END

#endif//INCLUDE_MUTEX_INNER_H
