#ifndef INCLUDE_SCOPED_LOCK_H
#define INCLUDE_SCOPED_LOCK_H

#include "foundation.hpp"

NAMESPACE_KT_BEGIN

/// A class that simplifies thread synchronization
/// with a mutex.
/// The constructor accepts a Mutex (and optionally
/// a timeout value in milliseconds) and locks it.
/// The destructor unlocks the mutex.
template <class M>
class KT_API ScopedLock
{
public:
    explicit ScopedLock(M& mutex): _mutex(mutex)
    {
        _mutex.lock();
    }

    ScopedLock(M& mutex, long milliseconds): _mutex(mutex)
    {
        _mutex.lock(milliseconds);
    }

    ~ScopedLock()
    {
        try
        {
            _mutex.unlock();
        }
        catch (...)
        {
        }
    }

private:
    M& _mutex;

    ScopedLock();
    ScopedLock(const ScopedLock&);
    ScopedLock& operator = (const ScopedLock&);
};


/// A class that simplifies thread synchronization
/// with a mutex.
/// The constructor accepts a Mutex (and optionally
/// a timeout value in milliseconds) and locks it.
/// The destructor unlocks the mutex.
/// The unlock() member function allows for manual
/// unlocking of the mutex.
template <class M>
class KT_API ScopedLockWithUnlock
{
public:
    explicit ScopedLockWithUnlock(M& mutex): _pMutex(&mutex)
    {
        _pMutex->lock();
    }

    ScopedLockWithUnlock(M& mutex, long milliseconds): _pMutex(&mutex)
    {
        _pMutex->lock(milliseconds);
    }

    ~ScopedLockWithUnlock()
    {
        try
        {
            unlock();
        }
        catch (...)
        {
        }
    }

    void unlock()
    {
        if (_pMutex)
        {
            _pMutex->unlock();
            _pMutex = 0;
        }
    }

private:
    M *_pMutex;

    ScopedLockWithUnlock();
    ScopedLockWithUnlock(const ScopedLockWithUnlock&);
    ScopedLockWithUnlock& operator = (const ScopedLockWithUnlock&);
};

NAMESPACE_KT_END

#endif//INCLUDE_SCOPED_LOCK_H
