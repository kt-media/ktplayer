#ifndef INCLUDE_MUTEX_H
#define INCLUDE_MUTEX_H

#include "mutex_inner.h"

NAMESPACE_KT_BEGIN

class KT_API Mutex: private MutexInner
{
public:
    using ScopedLock = ScopedLock<Mutex>;

    Mutex(): MutexInner() {}
    ~Mutex() {}

    inline void lock()
    {
        lockInner();
    }

    inline void lock(long milliseconds)
    {
        lockInner(milliseconds);
    }

    inline void unlock()
    {
        unlockInner();
    }

    inline bool tryLock()
    {
        return tryLockInner();
    }

    inline bool tryLock(long milliseconds)
    {
        return tryLockInner(milliseconds);
    }
};


//----------------------------------------------------------------------------------
class KT_API FastMutex: private MutexInner
{
public:
    using ScopedLock = ScopedLock<FastMutex>;

    FastMutex(): MutexInner(true) {}
    ~FastMutex() {}

    inline void lock()
    {
        lockInner();
    }

    inline void lock(long milliseconds)
    {
        lockInner(milliseconds);
    }

    inline void unlock()
    {
        unlockInner();
    }

    inline bool tryLock()
    {
        return tryLockInner();
    }

    inline bool tryLock(long milliseconds)
    {
        return tryLockInner(milliseconds);
    }

private:
    FastMutex             (const FastMutex&);
    FastMutex& operator = (const FastMutex&);
};


//----------------------------------------------------------------------------------
/// A NullMutex is an empty mutex implementation
/// which performs no locking at all. Useful in policy driven design
/// where the type of mutex used can be now a template parameter allowing the user to switch
/// between thread-safe and not thread-safe depending on his need
/// Works with the ScopedLock class
class KT_API NullMutex
{
public:
    using ScopedLock = ScopedLock<NullMutex>;

    NullMutex() {}
    ~NullMutex() {}

    void lock(long) {}
    bool tryLock()
    {
        return true;
    }

    bool tryLock(long)
    {
        return true;
    }

    void unlock() {}
};


//----------------------------------------------------------------------------------
#ifdef KT_HAVE_STD_ATOMICS
/// A SpinlockMutex, implemented in terms of std::atomic_flag, as
/// busy-wait mutual exclusion.
///
/// While in some cases (eg. locking small blocks of code)
/// busy-waiting may be an optimal solution, in many scenarios
/// spinlock may not be the right choice - it is up to the user to
/// choose the proper mutex type for their particular case.
///
/// Works with the ScopedLock class.
class KT_API SpinlockMutex
{
public:
    using ScopedLock = ScopedLock<SpinlockMutex>;

    /// Creates the SpinlockMutex.
    SpinlockMutex() = default;

    /// Destroys the SpinlockMutex.
    ~SpinlockMutex() = default;

    /// Locks the mutex. Blocks if the mutex
    /// is held by another thread.
    inline void lock()
    {
        while (_flag.test_and_set(std::memory_order_acquire));
    }

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread. Throws a TimeoutException
    /// if the mutex can not be locked within the given timeout.
    inline void lock(long milliseconds)
    {
        Timestamp now;
        Timestamp::TimeDiff diff(Timestamp::TimeDiff(milliseconds) * 1000);
        while (_flag.test_and_set(std::memory_order_acquire))
        {
            if (now.isElapsed(diff)) throw std::runtime_error("timeout");
        }
    }

    /// Tries to lock the mutex. Returns immediately, false
    /// if the mutex is already held by another thread, true
    /// if the mutex was successfully locked.
    inline bool tryLock()
    {
        return !_flag.test_and_set(std::memory_order_acquire);
    }

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread.
    /// Returns true if the mutex was successfully locked.
    inline bool tryLock(long milliseconds)
    {
        Timestamp now;
        Timestamp::TimeDiff diff(Timestamp::TimeDiff(milliseconds) * 1000);
        while (_flag.test_and_set(std::memory_order_acquire))
        {
            if (now.isElapsed(diff)) return false;
        }
        return true;
    }

    /// Unlocks the mutex so that it can be acquired by
    /// other threads.
    inline void unlock()
    {
        _flag.clear(std::memory_order_release);
    }

private:
    std::atomic_flag _flag = ATOMIC_FLAG_INIT;
};
#endif// KT_HAVE_STD_ATOMICS

NAMESPACE_KT_END

#endif//INCLUDE_MUTEX_H
