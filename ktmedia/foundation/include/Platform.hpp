#ifndef INCLUDE_PLATFORM_H
#define INCLUDE_PLATFORM_H

//
// Platform Identification
//
#define KT_OS_LINUX         0x0001
#define KT_OS_MAC_OS_X      0x0002
#define KT_OS_ANDROID       0x0003
#define KT_OS_UNKNOWN_UNIX  0x00ff

#if defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__) || defined(EMSCRIPTEN)
#   define KT_OS_FAMILY_UNIX 1
#   if defined(__ANDROID__)
#       define KT_OS KT_OS_ANDROID
#   else
#       define KT_OS KT_OS_LINUX
#   endif
#elif defined(__APPLE__) || defined(__TOS_MACOS__)
#   define KT_OS_FAMILY_UNIX       1
#   define KT_OS_FAMILY_BSD        1
#   define KT_OS                   KT_OS_MAC_OS_X
#endif



//
// Note: pthread_cond_timedwait() with CLOCK_MONOTONIC is supported
// on Linux and QNX, as well as on Android >= 5.0 (API level 21).
// On Android < 5.0, HAVE_PTHREAD_COND_TIMEDWAIT_MONOTONIC is defined
// to indicate availability of non-standard pthread_cond_timedwait_monotonic().
//
#ifndef KT_HAVE_MONOTONIC_PTHREAD_COND_TIMEDWAIT
#   if defined(__linux__) && !(defined(__ANDROID__) && (defined(HAVE_PTHREAD_COND_TIMEDWAIT_MONOTONIC) || __ANDROID_API__ <= 21))
#       define KT_HAVE_MONOTONIC_PTHREAD_COND_TIMEDWAIT 1
#   endif
#endif


#ifndef KT_HAVE_CLOCK_GETTIME
#   if defined(_POSIX_TIMERS) && defined(CLOCK_REALTIME)
#       ifndef __APPLE__ // See GitHub issue #1453 - not available before Mac OS 10.12/iOS 10
#           define KT_HAVE_CLOCK_GETTIME
#       endif
#   endif
#endif

#if defined(_POSIX_TIMEOUTS) && (_POSIX_TIMEOUTS - 200112L) >= 0L
#   if defined(_POSIX_THREADS) && (_POSIX_THREADS - 200112L) >= 0L
#       define KT_HAVE_MUTEX_TIMEOUT
#   endif
#endif


#if __cplusplus >= 201103L
#   ifndef KT_HAVE_STD_ATOMICS
#       define KT_HAVE_STD_ATOMICS
#   endif
#endif

#ifdef KT_HAVE_STD_ATOMICS
#   include <atomic>
#endif

#endif//INCLUDE_PLATFORM_H
