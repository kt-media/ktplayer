#ifndef INCLUDE_THREAD_H
#define INCLUDE_THREAD_H

#include "thread_posix.h"

NAMESPACE_KT_BEGIN

class KT_API Thread: private ThreadInner
{
public:
    using TID = ThreadInner::TID_INNER;
    using ThreadInner::Callable;

    Thread();
    Thread(const std::string&  name);
    ~Thread() = default;

    /// Sets the name of the thread.
    void setName(const char *name);
    /// Returns the name of the thread.
    std::string getName();

    /// Returns the thread's priority.
    Priority getPriority() const;
    /// Sets the thread's priority.
    void setPriority(Priority prio);

    /// Sets the thread's priority, using an operating system specific
    /// priority value. Use getMinOSPriority() and getMaxOSPriority() to
    /// obtain mininum and maximum priority values. Additionally,
    /// a scheduling policy can be specified. The policy is currently
    /// only used on POSIX platforms where the values SCHED_OTHER (default),
    /// SCHED_FIFO and SCHED_RR are supported.
    void setOSPriority(int prio, int policy = THREAD_POLICY_DEFAULT);

    /// Returns the thread's priority, expressed as an operating system
    /// specific priority value.
    /// May return 0 if the priority has not been explicitly set.
    int getOSPriority() const;

    /// Wakes up the thread which is in the state of interruptible
    /// sleep. For threads that are not suspended, calling this
    /// function has the effect of preventing the subsequent
    /// trySleep() call to put thread in a suspended state.
    void wakeUp();

    /// Sets the thread's stack size in bytes.
    /// Setting the stack size to 0 will use the default stack size.
    /// Typically, the real stack size is rounded up to the nearest
    /// page size multiple.
    void setStackSize(int size);

    /// Returns the thread's stack size in bytes.
    /// If the default stack size is used, 0 is returned.
    int getStackSize() const;


    /// Starts the thread with the given target.
    ///
    /// Note that the given Runnable object must remain
    /// valid during the entire lifetime of the thread, as
    /// only a reference to it is stored internally.
    void start(Runnable& target);

    /// Starts the thread with the given target and parameter.
    void start(Callable target, void *pData = nullptr);

    /// Starts the thread with the given target.
    ///
    /// The Thread ensures that the given target stays
    /// alive while the thread is running.
    void start(SharedPtr<Runnable> target);

    /// Starts the thread with the given functor object or lambda.
    template <class Functor>
    void startFunc(const Functor& fn)
    {
        startInner(new FunctorRunnable<Functor>(fn));
    }

    /// Waits until the thread completes execution.
    /// If multiple threads try to join the same
    /// thread, the result is undefined.
    void join();

    /// Waits for at most the given interval for the thread
    /// to complete. Throws a TimeoutException if the thread
    /// does not complete within the specified time interval.
    void join(long milliseconds);

    /// Waits for at most the given interval for the thread
    /// to complete. Returns true if the thread has finished,
    /// false otherwise.
    bool tryJoin(long milliseconds);

    /// Returns the unique thread ID of the thread.
    int id() const;

    /// Returns the native thread ID of the thread.
    TID tid() const;

protected:
    /// Creates a unique name for a thread.
    std::string makeName();

public:
    /**
     * Returns the minimum operating system-specific priority value,
     * which can be passed to setOSPriority() for the given policy.
     */
    static int getMinOSPriority(int policy = THREAD_POLICY_DEFAULT);

    /**
     * Returns the maximum operating system-specific priority value,
     * which can be passed to setOSPriority() for the given policy.
     */
    static int getMaxOSPriority(int policy = THREAD_POLICY_DEFAULT);

    /**
     * Suspends the current thread for the specified amount of time.
     */
    static void sleep(long milliseconds);

    /**
     * Starts an interruptible sleep. When trySleep() is called,
     * the thread will remain suspended until:
     *       - the timeout expires or
     *       - wakeUp() is called
     *
     * Function returns true if sleep attempt was completed, false
     * if sleep was interrupted by a wakeUp() call.
     * A frequent scenario where trySleep()/wakeUp() pair of functions
     * is useful is with threads spending most of the time idle,
     * with periodic activity between the idle times; trying to sleep
     * (as opposed to sleeping) allows immediate ending of idle thread
     * from the outside.
     *
     * The trySleep() and wakeUp() calls should be used with
     * understanding that the suspended state is not a true sleep,
     * but rather a state of waiting for an event, with timeout
     * expiration. This makes order of calls significant; calling
     * wakeUp() before calling trySleep() will prevent the next
     * trySleep() call to actually suspend the thread (which, in
     * some scenarios, may be desirable behavior).
     */
    static bool trySleep(long milliseconds);

    /**
     * Yields cpu to other threads.
     */
    static void yield();

    /**
     * Returns the Thread object for the currently active thread.
     * If the current thread is the main thread, 0 is returned.
     */
    static Thread *current();

    /**
     * Creates and returns a unique id for a thread.
     */
    static AtomicCounter::ValueType uniqueId();

private:
    Thread             (const Thread&);
    Thread& operator = (const Thread&);

private:
    Event                     _event;
    AtomicCounter::ValueType  _id;
    std::string               _name;
    mutable FastMutex         _mutex;
};

NAMESPACE_KT_END

#endif//INCLUDE_THREAD_H
