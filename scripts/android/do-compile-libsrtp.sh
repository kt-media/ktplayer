#! /usr/bin/env bash
set -e

SCRIPT_NAME=$0
   ROOT_DIR=$1
 SCRIPT_DIR=$2
ANDROID_ABI=$3
TARGET_NAME=$4
 BUILD_SKIP=false
  SDK_LEVEL=18

echo
echo "********** < ${SCRIPT_NAME} > **********"

if [ "$BUILD_SKIP" == "true" ]; then
    exit 0
fi

# load the android common env
source ${SCRIPT_DIR}/tools/env-common.sh
source ${SCRIPT_DIR}/tools/download.sh

if [ "$ANDROID_ABI" == "arm64-v8a" ] || [ "$ANDROID_ABI" == "x86_64" ]; then
    SDK_LEVEL=21
fi

# set link CC CXX AR env
set_android_toolchain ${ANDROID_ABI} ${SDK_LEVEL} ${ROOT_DIR}

# source base information
SRC_ORIG_DIR="$(get_deps_dir ${ROOT_DIR} ${TARGET_NAME})"

# source address
SRC_REMOTE_URL=https://github.com/cisco/libsrtp/archive/v2.3.0.tar.gz
      SRC_NAME=v2.3.0.tar.gz
      SRC_FILE=${SRC_ORIG_DIR}/${SRC_NAME}

# check the original souce exits
# maybe try to download the original source
download ${SRC_REMOTE_URL} ${SRC_ORIG_DIR} ${SRC_NAME}

# get the src to target directory
CONTRIB_DIR=${ROOT_DIR}/contrib/${ANDROID_ABI}/${TARGET_NAME}
rm    -rf ${CONTRIB_DIR}
mkdir -p  ${CONTRIB_DIR}
tar zxf ${SRC_FILE} --strip-components 1 -C ${CONTRIB_DIR}

# install dir
INSTALL_DIR=$(get_install_dir ${ROOT_DIR} ${ANDROID_ABI})

# goto target source directory
cd ${CONTRIB_DIR}
mkdir -p android-build-dir
cd       android-build-dir

# -DOPENSSL_CRYPTO_LIBRARY=${INSTALL_DIR}/lib/libcrypto.so \
# -DOPENSSL_CRYPTO_LIBRARY=${INSTALL_DIR}/lib/libcrypto.a \

# detach the openssl
OPENSSL_USED=""
if [ -f "${INSTALL_DIR}/lib/libcrypto.so" ]; then
    echo "INFO: use shared crypto library"
    OPENSSL_USED="${OPENSSL_USED} -DENABLE_OPENSSL=1"
    OPENSSL_USED="${OPENSSL_USED} -DOPENSSL_INCLUDE_DIR=${INSTALL_DIR}/include"
    OPENSSL_USED="${OPENSSL_USED} -DOPENSSL_CRYPTO_LIBRARY=${INSTALL_DIR}/lib/libcrypto.so"
elif [ -f "${INSTALL_DIR}/lib/libcrypto.a" ]; then
    echo "INFO: use static crypto library"
    OPENSSL_USED="${OPENSSL_USED} -DENABLE_OPENSSL=1"
    OPENSSL_USED="${OPENSSL_USED} -DOPENSSL_INCLUDE_DIR=${INSTALL_DIR}/include"
    OPENSSL_USED="${OPENSSL_USED} -DOPENSSL_CRYPTO_LIBRARY=${INSTALL_DIR}/lib/libcrypto.a"
else
    echo "INFO: no openssl, disable it"
    OPENSSL_USED="${OPENSSL_USED} -DENABLE_OPENSSL=0"
fi

cmake_dir_bin="$(get_android_cmake_dir)/bin/cmake"
${cmake_dir_bin} -G"Unix Makefiles" \
    -DANDROID_NDK=${ANDROID_NDK} \
    -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK}/build/cmake/android.toolchain.cmake \
    -DANDROID_ABI=${ANDROID_ABI} \
    -DANDROID_PLATFORM=${SDK_LEVEL} \
    ${OPENSSL_USED} \
    -DBUILD_SHARED_LIBS=1 \
    -DTEST_APPS=0 \
    -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
    ..

make -j$(get_cpu_count)
make install/strip


# the last one code
# back to the build script directory
cd ${SCRIPT_DIR}
