#! /usr/bin/env bash

set -e

if [ -z "$ANDROID_ABI" ]; then
    echo "ERROR: empty evn ANDROID_ABI checkout it"
    exit 1
fi

# Configuration options:
COMMON_FF_CFG_FLAGS="--enable-runtime-cpudetect \
 --disable-gray \
 --disable-swscale-alpha"

# Program options:
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-programs \
 --disable-ffmpeg \
 --disable-ffplay \
 --disable-ffprobe"

# Documentation options:
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-doc \
 --disable-htmlpages \
 --disable-manpages \
 --disable-podpages \
 --disable-txtpages"

# Component options:
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-avdevice \
 --enable-avcodec \
 --enable-avformat \
 --enable-avutil \
 --enable-swresample \
 --enable-swscale \
 --disable-postproc \
 --enable-avfilter \
 --disable-avresample \
 --enable-network"

# Hardware accelerators:
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-d3d11va \
 --disable-dxva2 \
 --disable-vaapi \
 --disable-vdpau \
 --disable-videotoolbox"

# Individual component options:
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-encoders \
 --enable-encoder=png \
 --enable-encoder=libx264 \
 --enable-encoder=aac"

# ./configure --list-decoders
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-decoders \
 --enable-decoder=aac \
 --enable-decoder=aac_latm \
 --enable-decoder=flv \
 --enable-decoder=h264 \
 --enable-decoder=vp6f \
 --enable-decoder=flac \
 --enable-decoder=hevc \
 --enable-decoder=vp8 \
 --enable-decoder=vp9 \
 --enable-decoder=mpeg4 \
 --disable-hwaccels"

#  --enable-decoder=mp3* \


# ./configure --list-muxers
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-muxers \
 --enable-muxer=mp4"

# ./configure --list-demuxers
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-demuxers \
 --enable-demuxer=aac \
 --enable-demuxer=concat \
 --enable-demuxer=data \
 --enable-demuxer=flv \
 --enable-demuxer=hls \
 --enable-demuxer=live_flv \
 --enable-demuxer=mov \
 --enable-demuxer=mp3 \
 --enable-demuxer=mpegps \
 --enable-demuxer=mpegts \
 --enable-demuxer=mpegvideo \
 --enable-demuxer=flac \
 --enable-demuxer=hevc \
 --enable-demuxer=webm_dash_manifest \
 --enable-demuxer=avi"

# ./configure --list-parsers
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-parsers \
 --enable-parser=aac \
 --enable-parser=aac_latm \
 --enable-parser=h264 \
 --enable-parser=flac \
 --enable-parser=hevc"

# ./configure --list-bsf
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --enable-bsfs \
 --disable-bsf=chomp \
 --disable-bsf=dca_core \
 --disable-bsf=dump_extradata \
 --disable-bsf=hevc_mp4toannexb \
 --disable-bsf=imx_dump_header \
 --disable-bsf=mjpeg2jpeg \
 --disable-bsf=mjpega_dump_header \
 --disable-bsf=mov2textsub \
 --disable-bsf=mp3_header_decompress \
 --disable-bsf=mpeg4_unpack_bframes \
 --disable-bsf=noise \
 --disable-bsf=remove_extradata \
 --disable-bsf=text2movsub \
 --disable-bsf=vp9_superframe \
 --disable-bsf=eac3_core"

# ./configure --list-protocols
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --enable-protocols \
 --enable-protocol=async \
 --disable-protocol=bluray \
 --disable-protocol=concat \
 --disable-protocol=crypto \
 --disable-protocol=ffrtmpcrypt \
 --enable-protocol=ffrtmphttp \
 --disable-protocol=gopher \
 --disable-protocol=icecast \
 --disable-protocol=librtmp* \
 --disable-protocol=libssh \
 --disable-protocol=md5 \
 --disable-protocol=mmsh \
 --disable-protocol=mmst \
 --disable-protocol=rtmp* \
 --enable-protocol=rtmp \
 --enable-protocol=rtmpt \
 --disable-protocol=rtp \
 --disable-protocol=sctp \
 --disable-protocol=srtp \
 --disable-protocol=subfile \
 --disable-protocol=unix \
 --disable-devices \
 --disable-filters"

# External library support:
## COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
##  --disable-iconv \
##  --disable-audiotoolbox \
##  --disable-videotoolbox"

# Developer options (useful when working on FFmpeg itself):
COMMON_FF_CFG_FLAGS="${COMMON_FF_CFG_FLAGS} \
 --disable-linux-perf \
 --disable-bzlib"

# For MeiCam SDK
# for android
FF_EXTRA_CFG="${FF_EXTRA_CFG} \
 --enable-decoder=mjpeg \
 --enable-decoder=mpeg1video \
 --enable-decoder=mpegvideo \
 --enable-decoder=h261 \
 --enable-decoder=h263 \
 --enable-decoder=dnxhd \
 --enable-decoder=cfhd \
 --enable-decoder=msmpeg4v1 \
 --enable-decoder=msmpeg4v2 \
 --enable-decoder=msmpeg4v3 \
 --enable-decoder=rv10 \
 --enable-decoder=rv20 \
 --enable-decoder=rv30 \
 --enable-decoder=rv40 \
 --enable-decoder=wmalossless \
 --enable-decoder=wmapro \
 --enable-decoder=wmav1 \
 --enable-decoder=wmav2 \
 --enable-decoder=wmavoice \
 --enable-demuxer=xwma \
 --enable-decoder=vc1image \
 --enable-decoder=mss2 \
 --enable-decoder=als \
 --enable-decoder=dst \
 --enable-decoder=pcm_alaw \
 --enable-decoder=pcm_mulaw \
 --enable-decoder=cook \
 --enable-decoder=sipr \
 --enable-decoder=ape \
 --enable-demuxer=rm \
 --enable-demuxer=mpegts \
 --enable-demuxer=mpegtsraw \
 --enable-demuxer=mjpeg \
 --enable-demuxer=mjpeg_2000 \
 --enable-parser=mpegvideo \
 --enable-parser=mpegaudio \
 --enable-parser=ac3 \
 --enable-parser=rv30 \
 --enable-parser=rv40 \
 --enable-parser=flac \
 --enable-parser=h261 \
 --enable-parser=h263 \
 --enable-parser=cook \
 --enable-parser=vc1 \
 --enable-parser=dirac \
 --enable-parser=dnxhd \
 --enable-parser=mjpeg \
 --enable-demuxer=asf \
 --enable-muxer=ipod \
 --enable-muxer=mp3 \
 --enable-muxer=avi \
 --enable-muxer=flv \
 --enable-muxer=rm \
 --enable-muxer=mp4_muxer \
 --enable-muxer=ipod_muxer \
 --enable-mediacodec \
 --enable-jni \
 --enable-pic"

# --enable-decoder=mjpeg* \
# --enable-decoder=mp2* \
# --enable-decoder=wmv1 \
# --enable-decoder=wmv2 \
# --enable-decoder=wmv3 \
# --enable-decoder=wmv3_crystalhd \
# --enable-decoder=wmv3image \


if [ -z "$FF_CFLAGS" ]; then
    echo "ERROR: empty evn FF_CFLAGS checkout it"
    exit 1
fi

if [ -z "$FF_LDFLAGS" ]; then
    echo "ERROR: empty evn FF_LDFLAGS checkout it"
    #exit 1
fi

if [ -z "$FF_CONFIG_FLAG" ]; then
    echo "ERROR: empty evn FF_CONFIG_FLAG checkout it"
    exit 1
fi


FF_CFLAGS="${FF_CFLAGS} -I${INSTALL_DIR}/include"

if   [ "$ANDROID_ABI" = "armeabi-v7a" ]; then
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --arch=arm --cpu=cortex-a8"
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-neon --enable-thumb"
         FF_CFLAGS="${FF_CFLAGS} -march=armv7-a -mcpu=cortex-a8 -mfpu=vfpv3-d16 -mfloat-abi=softfp -mthumb"
        FF_LDFLAGS="${FF_LDFLAGS} -Wl,--fix-cortex-a8"
elif [ "$ANDROID_ABI" = "arm64-v8a" ]; then
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --arch=aarch64 --enable-yasm --enable-neon"
    #FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --arch=aarch64 --enable-neon"
elif [ "$ANDROID_ABI" = "x86" ]; then
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --arch=x86 --cpu=i686 --enable-yasm"
         FF_CFLAGS="${FF_CFLAGS} -march=atom -msse3 -ffast-math -mfpmath=sse"
elif [ "$ANDROID_ABI" = "x86_64" ]; then
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --arch=x86_64 --enable-yasm"
else
    echo "unknown ANDROID_ABI $ANDROID_ABI";
    exit 1
fi

#
# check and attach libraries

if [ -z "$LD_DEPS" ]; then
    echo "ERROR: empty evn LD_DEPS checkout it"
    exit 1
fi

if [ -z "$INSTALL_DIR" ]; then
    echo "ERROR: empty evn INSTALL_DIR checkout it"
    exit 1
fi


LD_DEPS="${FF_LDFLAGS} -L${INSTALL_DIR}/lib"

#
# openssl
#
if [ -f "${INSTALL_DIR}/lib/libssl.a" ] && [ -f "${INSTALL_DIR}/lib/libcrypto.a" ]; then
    echo "INFO: enable openssl"
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-openssl"
           LD_DEPS="${LD_DEPS}        -lssl -lcrypto"
elif [ -f "${INSTALL_DIR}/lib/libssl.so" ] && [ -f "${INSTALL_DIR}/lib/libcrypto.so" ]; then
    echo "INFO: enable openssl"
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-openssl"
           LD_DEPS="${LD_DEPS}        -lssl -lcrypto"
else
    echo "INFO: no any openssl library"
fi

#
# libsoxr
#
if [ -f "${INSTALL_DIR}/lib/libsoxr.so" ]; then
    echo "INFO: enable libsoxr"
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-libsoxr"
           LD_DEPS="${LD_DEPS}        -lsoxr"
fi

#
# x264  x265
#
ENABLE_LIBX26x=false
if [ -f "${INSTALL_DIR}/lib/libx264.a" ] || [ -f "${INSTALL_DIR}/lib/libx264.so" ]; then
    ENABLE_LIBX26x=true
    echo "INFO: enable libx264"
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-libx264"
           LD_DEPS="${LD_DEPS}        -lx264"
fi

#no support
if [ -f "${INSTALL_DIR}/lib/libx265.a" ] || [ -f "${INSTALL_DIR}/lib/libx265.so" ]; then
    ENABLE_LIBX26x=true
    echo "INFO: enable libx265"
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-libx265"
           LD_DEPS="${LD_DEPS}        -lx265"
fi  

if [ "${ENABLE_LIBX26x}" == "true" ]; then
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-gpl --enable-nonfree"
           LD_DEPS="${LD_DEPS}        -lm -lstdc++"
fi


#
# libsrt not support
#
if [ -f "${INSTALL_DIR}/lib/libsrt.so" ] || [ -f "${INSTALL_DIR}/lib/libsrt.a" ]; then
    echo "INFO: enable libsrt"
    FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-libsrt"
           LD_DEPS="${LD_DEPS}        -lsrt -lm -lstdc++"
fi
