#! /usr/bin/env bash
set -e

SCRIPT_NAME=$0
   ROOT_DIR=$1
 SCRIPT_DIR=$2
ANDROID_ABI=$3
TARGET_NAME=$4
 BUILD_SKIP=false
  SDK_LEVEL=18

echo
echo "********** < ${SCRIPT_NAME} > **********"

if [ "$BUILD_SKIP" == "true" ]; then
    exit 0
fi

# load the android common env
source ${SCRIPT_DIR}/tools/env-common.sh
source ${SCRIPT_DIR}/tools/download.sh

if [ "$ANDROID_ABI" == "arm64-v8a" ] || [ "$ANDROID_ABI" == "x86_64" ]; then
    SDK_LEVEL=21
fi

# set link CC CXX AR env
set_android_toolchain ${ANDROID_ABI} ${SDK_LEVEL} ${ROOT_DIR}

# source base information
SRC_ORIG_DIR="$(get_deps_dir ${ROOT_DIR} ${TARGET_NAME})"

# source address
SRC_REMOTE_URL=https://gitee.com/aidem/libyuv.git
   SRC_VERSION=master

# clone the source
gitclone ${SRC_REMOTE_URL} ${SRC_ORIG_DIR} ${SRC_VERSION}

# get the src to target directory
CONTRIB_DIR=${ROOT_DIR}/contrib/${ANDROID_ABI}/${TARGET_NAME}
rm    -rf ${CONTRIB_DIR}
mkdir -p  ${CONTRIB_DIR}

git -C ${SRC_ORIG_DIR} archive --format zip --output "${CONTRIB_DIR}/${TARGET_NAME}.zip" ${SRC_VERSION}
unzip -q  ${CONTRIB_DIR}/${TARGET_NAME}.zip -d ${CONTRIB_DIR}
rm    -rf ${CONTRIB_DIR}/${TARGET_NAME}.zip

# install dir
INSTALL_DIR=$(get_install_dir ${ROOT_DIR} ${ANDROID_ABI})

# goto target source directory
cd ${CONTRIB_DIR}
mkdir android-build-dir
cd    android-build-dir

cmake_dir_bin="$(get_android_cmake_dir)/bin/cmake"
${cmake_dir_bin} -G"Unix Makefiles" \
    -DANDROID_NDK=${ANDROID_NDK} \
    -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK}/build/cmake/android.toolchain.cmake \
    -DANDROID_ABI=${ANDROID_ABI} \
    -DANDROID_PLATFORM=${SDK_LEVEL} \
    -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
    ..

make depend
make -j$(get_cpu_count) yuv
make install/strip

# the last one code
# back to the build script directory
cd ${SCRIPT_DIR}
