#! /usr/bin/env bash
set -e

SCRIPT_NAME=$0
   ROOT_DIR=$1
 SCRIPT_DIR=$2
ANDROID_ABI=$3
TARGET_NAME=$4
 BUILD_SKIP=false
  SDK_LEVEL=18

echo
echo "********** < ${SCRIPT_NAME} > **********"

if [ "$BUILD_SKIP" == "true" ]; then
    exit 0
fi

# load the android common env
source ${SCRIPT_DIR}/tools/env-common.sh
source ${SCRIPT_DIR}/tools/download.sh

if [ "$ANDROID_ABI" == "arm64-v8a" ] || [ "$ANDROID_ABI" == "x86_64" ]; then
    SDK_LEVEL=21
fi

# set link CC CXX AR env
set_android_toolchain ${ANDROID_ABI} ${SDK_LEVEL} ${ROOT_DIR}
# print the base information
## print_android_toolchain_env

# source base information
SRC_ORIG_DIR="$(get_deps_dir ${ROOT_DIR} ${TARGET_NAME})"

# source address
SRC_REMOTE_URL=https://www.openssl.org/source/openssl-1.1.1g.tar.gz
      SRC_NAME=openssl-1.1.1g.tar.gz
      SRC_FILE=${SRC_ORIG_DIR}/${SRC_NAME}

# check the original souce exits
# maybe try to download the original source
download ${SRC_REMOTE_URL} ${SRC_ORIG_DIR} ${SRC_NAME}

# unzip the target source to dest dir
CONTRIB_DIR=${ROOT_DIR}/contrib/${ANDROID_ABI}/${TARGET_NAME}
rm   -rf ${CONTRIB_DIR}
mkdir -p ${CONTRIB_DIR}
tar zxf ${SRC_FILE} --strip-components 1 -C ${CONTRIB_DIR}

# install dir
INSTALL_DIR=$(get_install_dir ${ROOT_DIR} ${ANDROID_ABI})


# goto the target source directory
cd ${CONTRIB_DIR}

# configure openssl
ndk_prefix=$(get_arch_prefix ${ANDROID_ABI})
openssl_arch=
if [ "$ANDROID_ABI" = "armeabi-v7a" ]; then
    openssl_arch="android-arm"
elif [ "$ANDROID_ABI" = "arm64-v8a" ]; then
    openssl_arch="android-arm64"
elif [ "$ANDROID_ABI" = "x86" ]; then
    openssl_arch="android-x86"
elif [ "$ANDROID_ABI" = "x86_64" ]; then
    openssl_arch="android-x86_64"
fi

./Configure $openssl_arch -D__ANDROID_API__=${SDK_LEVEL} \
    --openssldir=${INSTALL_DIR} \
    --prefix=${INSTALL_DIR} \
    --cross-compile-prefix=${ndk_prefix}- \
    zlib-dynamic \
    -shared no-ssl2 no-ssl3 no-comp no-hw no-engine

if [ -f "Makefile" ]; then
    mv Makefile Makefile.bk
    ${ROOT_DIR}/scripts/android/cmake/ssl/replace.py Makefile.bk ./Makefile
fi

make -j$(get_cpu_count) install_dev

# disable static library
BACKUP_DIR="${INSTALL_DIR}/lib/back-up"
mkdir -p ${BACKUP_DIR}

if [ -f "${INSTALL_DIR}/lib/libcrypto.so" ]; then
    if [ -f "${INSTALL_DIR}/lib/libcrypto.a" ]; then
        mv ${INSTALL_DIR}/lib/libcrypto.a ${BACKUP_DIR}
    fi
    ${STRIP} ${INSTALL_DIR}/lib/libcrypto.so
fi

if [ -f "${INSTALL_DIR}/lib/libssl.so" ]; then
    if [ -f "${INSTALL_DIR}/lib/libssl.a" ]; then
        mv ${INSTALL_DIR}/lib/libssl.a ${BACKUP_DIR}
    fi
    ${STRIP} ${INSTALL_DIR}/lib/libssl.so
fi


# the last one code
# back to the build script directory
cd ${SCRIPT_DIR}
