#! /usr/bin/env bash
set -e

# checkout android SDK and NDK
source ./tools/env-detect.sh

SCRIPT_NAME=$0
   ROOT_DIR=$1
 SCRIPT_DIR=$(pwd)

echo " script dir: ${SCRIPT_DIR}"
echo "   root dir: ${ROOT_DIR}"
echo "script name: ${SCRIPT_NAME}"
echo "--------------------------------------------------------------------------------"

# remove all old build directory
rm    -rf ${ROOT_DIR}/build
mkdir  -p ${ROOT_DIR}/build


ABIS="arm64-v8a armeabi-v7a x86_64 x86"
#ABIS="arm64-v8a armeabi-v7a"
#ABIS="arm64-v8a "

TARGETS="ssl srt libsrtp libsoxr libyuv soundtouch x264 x265 ffmpeg"
#TARGETS="ssl srt libsrtp libsoxr libyuv soundtouch x264 ffmpeg"
#TARGETS="ffmpeg"

for abi in ${ABIS};
do
    for target in ${TARGETS};
    do
        sh ${SCRIPT_DIR}/do-compile-${target}.sh ${ROOT_DIR} ${SCRIPT_DIR} ${abi} ${target}
    done
done


#
# do-compile-ssl.sh
# do-compile-srt.sh
# do-compile-libsrtp.sh
# do-compile-x264.sh
# do-compile-x265.sh
# do-compile-libsoxr.sh
# do-compile-libyuv.sh
# do-compile-soundtouch.sh
# do-compile-ffmpeg.sh





