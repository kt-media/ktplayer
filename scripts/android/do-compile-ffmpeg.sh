#! /usr/bin/env bash
set -e

SCRIPT_NAME=$0
   ROOT_DIR=$1
 SCRIPT_DIR=$2
ANDROID_ABI=$3
TARGET_NAME=$4
 BUILD_SKIP=false
  SDK_LEVEL=18

echo
echo "********** < ${SCRIPT_NAME} > **********"

if [ "$BUILD_SKIP" == "true" ]; then
    exit 0
fi

# load the android common env
source ${SCRIPT_DIR}/tools/env-common.sh
source ${SCRIPT_DIR}/tools/download.sh

if [ "$ANDROID_ABI" == "arm64-v8a" ] || [ "$ANDROID_ABI" == "x86_64" ]; then
    SDK_LEVEL=21
fi

# set link CC CXX AR env
set_android_toolchain ${ANDROID_ABI} ${SDK_LEVEL} ${ROOT_DIR}
# print the base information
## print_android_toolchain_env

# source base information
SRC_ORIG_DIR="$(get_deps_dir ${ROOT_DIR} ${TARGET_NAME})"

###   # source address
###   # http://ffmpeg.org/releases/ffmpeg-4.2.4.tar.gz
###   # http://ffmpeg.org/releases/ffmpeg-4.3.1.tar.gz
###   # 
###   SRC_REMOTE_URL=http://ffmpeg.org/releases/ffmpeg-4.2.4.tar.gz
###         SRC_NAME=ffmpeg-4.2.4.tar.gz
###         SRC_FILE=${SRC_ORIG_DIR}/${SRC_NAME}
###   
###   # check the original souce exits
###   # maybe try to download the original source
###   download ${SRC_REMOTE_URL} ${SRC_ORIG_DIR} ${SRC_NAME}
###   
###   # unzip the target source to dest dir
###   CONTRIB_DIR=${ROOT_DIR}/contrib/${ANDROID_ABI}/${TARGET_NAME}
###    rm   -rf ${CONTRIB_DIR}
###   mkdir -p ${CONTRIB_DIR}
###   tar zxf  ${SRC_FILE} --strip-components 1 -C ${CONTRIB_DIR}


# source address
SRC_REMOTE_URL=https://gitee.com/aidem/FFmpeg.git
   SRC_VERSION=release/4.3

# clone the source
gitclone ${SRC_REMOTE_URL} ${SRC_ORIG_DIR} ${SRC_VERSION}

# get the src to target directory
CONTRIB_DIR=${ROOT_DIR}/contrib/${ANDROID_ABI}/${TARGET_NAME}
rm    -rf ${CONTRIB_DIR}
mkdir -p  ${CONTRIB_DIR}

git -C ${SRC_ORIG_DIR} archive --format zip --output "${CONTRIB_DIR}/${TARGET_NAME}.zip" ${SRC_VERSION}
unzip -q  ${CONTRIB_DIR}/${TARGET_NAME}.zip -d ${CONTRIB_DIR}
rm    -rf ${CONTRIB_DIR}/${TARGET_NAME}.zip

# install dir
INSTALL_DIR=$(get_install_dir ${ROOT_DIR} ${ANDROID_ABI})


FF_CONFIG_FLAG=" "
FF_CFLAGS="-std=c99 -O3 -Wall -pipe -fstrict-aliasing -Werror=strict-aliasing -ffast-math -DANDROID -DNDEBUG"
FF_LDFLAGS=" "
LD_DEPS=" "
# source ffmpeg config file
source ${SCRIPT_DIR}/config/ffmpeg/config-ffmpeg.sh

# link all flags
FF_CONFIG_FLAG="${FF_CONFIG_FLAG} ${COMMON_FF_CFG_FLAGS} ${FF_EXTRA_CFG}"
FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-optimizations"
FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --enable-small"
FF_CONFIG_FLAG="${FF_CONFIG_FLAG} --disable-asm"
     FF_CFLAGS="${FF_CFLAGS} "
FF_LDFLAGS="${FF_LDFLAGS} ${LD_DEPS}"

# goto the target source directory
cd ${CONTRIB_DIR}


echo
echo $(pkg-config --list-all|grep srt)
echo $(pkg-config --list-all|grep x264)
echo $(pkg-config --list-all|grep x265)
echo


host_prefix=$(get_arch_prefix ${ANDROID_ABI})
./configure \
    --target-os=android \
    --enable-cross-compile \
    --prefix="${INSTALL_DIR}" \
    ${FF_CONFIG_FLAG} \
    --disable-shared --enable-static \
    --cross-prefix=${host_prefix}- \
    --pkg-config="$(which pkg-config)" \
    --ar=$AR \
    --as=$AS \
    --nm=$NM \
    --ranlib=$RANLIB \
    --strip=$STRIP \
    --cc=$CC \
    --cxx=$CXX \
    --sysroot=${SYSROOT} \
    --extra-cflags="$FF_CFLAGS" \
    --extra-ldflags="$FF_LDFLAGS"

make -j$(get_cpu_count)
make install


ALL_OBJS=$(find ./ -name "*.o" -type f)

$CC -lm -lz -shared --sysroot=${SYSROOT} -Wl,--no-undefined -Wl,-z,noexecstack $FF_LDFLAGS \
    -Wl,-soname,libffmpeg.so \
    ${ALL_OBJS} $LD_DEPS \
    -o ${INSTALL_DIR}/lib/libffmpeg.so

BACKUP_DIR="${INSTALL_DIR}/lib/back-up"
mkdir -p ${BACKUP_DIR}
mv ${INSTALL_DIR}/lib/libavcodec.a    ${BACKUP_DIR}
mv ${INSTALL_DIR}/lib/libavfilter.a   ${BACKUP_DIR}
mv ${INSTALL_DIR}/lib/libavformat.a   ${BACKUP_DIR}
mv ${INSTALL_DIR}/lib/libavutil.a     ${BACKUP_DIR}
mv ${INSTALL_DIR}/lib/libswresample.a ${BACKUP_DIR}
mv ${INSTALL_DIR}/lib/libswscale.a    ${BACKUP_DIR}

$STRIP ${INSTALL_DIR}/lib/libffmpeg.so


# the last one code
# back to the build script directory
cd ${SCRIPT_DIR}


# 相关坑
# 1.  GNU assembler not found, install/update gas-preprocessor
#   解决方案  --disable-asm
#
# 2. ERROR: srt >= 1.3.0 not found using pkg-config
# 添加如下的环境变量
#    export PKG_CONFIG_SYSROOT_DIR="$install_dir"
#    export      PKG_CONFIG_LIBDIR="$PKG_CONFIG_SYSROOT_DIR/lib/pkgconfig"
#    export        PKG_CONFIG_PATH="$PKG_CONFIG_LIBDIR:$PKG_CONFIG_PATH"    --notused
# set export pkg_config=$(which pkg-config) OR
# --pkg-config="$(which pkg-config)"
#
# 3. ERROR: x265 not found using pkg-config
#    需要添加 -lm -lstdc++
#
# 4. libavformat/libsrt.c:317:66: error: use of undeclared identifier 'SRTO_STRICTENC'; did you mean 'SRTO_STATE'?
#  srt 版本是使用 master 分支的， SRTO_STRICTENC 太旧
#  其中一个需要更新或者降级
#  srt 其他老版本在 andrioid 上编译都存在问题
# how to fix? maybe time
#



