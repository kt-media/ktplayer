#! /usr/bin/env bash
set -e

# $1 source url
# $2 dst dir
# $3 name
function download() {
    local source=$1
    local dstDir=$2
    local   name=$3

    mkdir -p $dstDir

    if [ ! -f "$dstDir/$name" ]; then
        #curl -o $dstDir/$name $source
        wget -P $dstDir $source
    fi

    if [ ! -f "$dstDir/$name" ]; then
        exit 1
    fi
}


function gitclone() {
    local  source=$1
    local  dstDir=$2
    local version=$3

    # no source directory
    if [ ! -d "$dstDir/.git" ];then
        mkdir -p $dstDir
        git -c diff.mnemonicprefix=false -c core.quotepath=false clone --recursive ${source} ${dstDir}
    #else
    #    git -C $dstDir checkout
    #    git -C $dstDir reset HEAD^ --hard
    #    git -C $dstDir pull
    fi

    # checkout to tagart branch or tag
    git -C $dstDir checkout $version
}

