#! /usr/bin/env bash
set -e


function get_arch_prefix() {
    local   android_abi=$1
    case ${android_abi} in
        armeabi-v7a)
            echo "arm-linux-androideabi"
        ;;
        arm64-v8a)
            echo "aarch64-linux-android"
        ;;
        x86)
            echo "i686-linux-android"
        ;;
        x86_64)
            echo "x86_64-linux-android"
        ;;
    esac
}


#
# use at armv7a-linux-androideabi21-clang
# use at armv7a-linux-androideabi21-clang++
function get_arch_abi_prefix() {
    local   android_abi=$1
    case ${android_abi} in
        armeabi-v7a)
            echo "armv7a-linux-androideabi"
        ;;
        arm64-v8a)
            echo "aarch64-linux-android"
        ;;
        x86)
            echo "i686-linux-android"
        ;;
        x86_64)
            echo "x86_64-linux-android"
        ;;
    esac
}

function get_install_dir() {
    local    root_dir=$1
    local android_abi=$2
    local     out_dir=$root_dir/build/$android_abi
    mkdir -p $out_dir
    echo "$out_dir"
}

function get_android_cmake_dir() {
    echo "$(echo "${ANDROID_SDK}/cmake/"* tr ' ' '\n' | sort -n -k 2 -t - -r | cut -d'-' -f 2 | cut -d' ' -f 1)"
}

function get_deps_dir() {
    local    root_dir=$1
    local target_name=$2

    echo "${root_dir}/dependence/${target_name}"
}

function set_android_toolchain() {
    local   android_abi=$1
    local     sdk_level=$2
    local      root_dir=$3
    local install_dir=$(get_install_dir ${root_dir} ${android_abi})

    # configure pkg-config paths if inside buildscripts
    export PKG_CONFIG_SYSROOT_DIR="$install_dir"
    export      PKG_CONFIG_LIBDIR="$PKG_CONFIG_SYSROOT_DIR/lib/pkgconfig"
    #export        PKG_CONFIG_PATH="$PKG_CONFIG_LIBDIR:$PKG_CONFIG_PATH"

    local     base_prefix="$(get_arch_prefix     ${android_abi})"
    local base_abi_prefix="$(get_arch_abi_prefix ${android_abi})"
    local      llvm_chain="$(echo "$ANDROID_NDK/toolchains/llvm/prebuilt/"*)"
    local llvm_chain_arch="${llvm_chain}/${base_prefix}"
    local       gcc_chain="$(echo "$ANDROID_NDK/toolchains/${base_prefix}-4.9/prebuilt/"*)"
    local  gcc_chain_arch="${gcc_chain}/${base_prefix}"

    export   PATH="${llvm_chain}/bin:${llvm_chain_arch}/bin:${gcc_chain}/bin:${gcc_chain_arch}/bin:$ANDROID_NDK:$ANDROID_SDK/tools:$PATH"
    export      AR=${base_prefix}-ar
    export      CC=${base_abi_prefix}${sdk_level}-clang
    export     CXX=${base_abi_prefix}${sdk_level}-clang++
    export      AS=${base_prefix}-as
    export      LD=${base_prefix}-ld
    export      NM=${base_prefix}-nm
    export  RANLIB=${base_prefix}-ranlib
    export   STRIP=${base_prefix}-strip
    export SYSROOT=${llvm_chain}/sysroot
}

function get_sdk_home() {
    echo "$ANDROID_SDK"
}

function get_ndk_home() {
    echo "$ANDROID_NDK"
}

function get_cpu_count() {
    if [ "$(uname)" == "Darwin" ]; then
        echo $(sysctl -n hw.physicalcpu)
    else
        echo $(nproc)
    fi
}

function print_android_toolchain_env() {
    echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "        AR=${AR}"
    echo "        CC=${CC}"
    echo "       CXX=${CXX}"
    echo "        AS=${AS}"
    echo "        LD=${LD}"
    echo "        NM=${NM}"
    echo "    RANLIB=${RANLIB}"
    echo "     STRIP=${STRIP}"
    echo "      PATH=${PATH}"
    echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
}

