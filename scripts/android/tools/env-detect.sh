#! /usr/bin/env bash
set -e

 UNAME_S=$(uname -s)
UNAME_SM=$(uname -sm)

echo "--------------------------------------------------------------------------------"
echo "Current build operator system: $UNAME_SM"

if [ -z "$ANDROID_SDK" ]; then
    echo "ERROR: environment variable ANDROID_SDK not set."
    echo "--------------------------------------------------------------------------------"
    echo
    exit 1
fi

if [ -z "$ANDROID_NDK" ]; then
    echo "ERROR: environment variable ANDROID_NDK not set."
    echo "--------------------------------------------------------------------------------"
    echo
    exit 1
fi

PKG_CONFIG_PATH=$(which pkg-config)
if [[ -z ${PKG_CONFIG_PATH} ]]; then
    echo "ERROR: not found pkg-config in this ${UNAME_SM}"
    echo "--------------------------------------------------------------------------------"
    echo
    exit 1
fi

CURL_PATH=$(which curl)
if [[ -z ${CURL_PATH} ]]; then
    echo "ERROR: not found curl in this ${UNAME_SM}"
    echo "--------------------------------------------------------------------------------"
    echo
    exit 1
fi


echo "INFO: found ${PKG_CONFIG_PATH}"
echo "INFO: found ${CURL_PATH}"


# try to detect NDK version
NDK_VERSION=$(grep -o '^r[0-9]*.*' $ANDROID_NDK/RELEASE.TXT 2>/dev/null | sed 's/[[:space:]]*//g' | cut -b2-)
case "$NDK_VERSION" in
    10e*)
        echo "ERROR: Not support NDKr$NDK_VERSION, must NDKr16.1.4479499 or later"
        echo "--------------------------------------------------------------------------------"
        exit 1
    ;;
    *)
        NDK_VERSION=$(grep -o '^Pkg\.Revision.*=[0-9]*.*' $ANDROID_NDK/source.properties 2>/dev/null | sed 's/[[:space:]]*//g' | cut -d "=" -f 2)
        if test -d ${ANDROID_NDK}/toolchains/arm-linux-androideabi-4.9
        then
            echo "INFO: found NDKr$NDK_VERSION"
        else
            echo "ERROR: Not support NDKr$NDK_VERSION, must NDKr16.1.4479499 or later"
            echo "--------------------------------------------------------------------------------"
            exit 1
        fi
    ;;
esac

echo "--------------------------------------------------------------------------------"
