#! /usr/bin/env bash
set -e

SCRIPT_NAME=$0
   ROOT_DIR=$1
 SCRIPT_DIR=$2
ANDROID_ABI=$3
TARGET_NAME=$4
 BUILD_SKIP=false
  SDK_LEVEL=18

echo
echo "********** < ${SCRIPT_NAME} > **********"

if [ "$BUILD_SKIP" == "true" ]; then
    exit 0
fi


# load the android common env
source ${SCRIPT_DIR}/tools/env-common.sh
source ${SCRIPT_DIR}/tools/download.sh

if [ "$ANDROID_ABI" == "arm64-v8a" ] || [ "$ANDROID_ABI" == "x86_64" ]; then
    SDK_LEVEL=21
fi

# set link CC CXX AR env
set_android_toolchain ${ANDROID_ABI} ${SDK_LEVEL} ${ROOT_DIR}
# print the base information
## print_android_toolchain_env

# source base information
SRC_ORIG_DIR="$(get_deps_dir ${ROOT_DIR} ${TARGET_NAME})"

# source address
# https://github.com/videolan/x265/archive/3.4.tar.gz  3.4华为提交的 ipfilter8.S 编译不过
# https://github.com/videolan/x265/archive/3.3.tar.gz
SRC_REMOTE_URL=https://github.com/videolan/x265/archive/3.3.tar.gz
      SRC_NAME=3.3.tar.gz
      SRC_FILE=${SRC_ORIG_DIR}/${SRC_NAME}

# check the original souce exits
# maybe try to download the original source
download ${SRC_REMOTE_URL} ${SRC_ORIG_DIR} ${SRC_NAME}

# unzip the target source to dest dir
CONTRIB_DIR=${ROOT_DIR}/contrib/${ANDROID_ABI}/${TARGET_NAME}
rm   -rf ${CONTRIB_DIR}
mkdir -p ${CONTRIB_DIR}
tar zxf ${SRC_FILE} --strip-components 1 -C ${CONTRIB_DIR}

# install dir
INSTALL_DIR=$(get_install_dir ${ROOT_DIR} ${ANDROID_ABI})


# goto target source directory
cd ${CONTRIB_DIR}
mkdir -p android-build-dir
cd       android-build-dir

cmake_dir_bin="$(get_android_cmake_dir)/bin/cmake"
${cmake_dir_bin} -G"Unix Makefiles" \
    -DCMAKE_SYSTEM_NAME=Android \
    -DANDROID_NDK=${ANDROID_NDK} \
    -DANDROID_ABI=${ANDROID_ABI} \
    -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK}/build/cmake/android.toolchain.cmake \
    -DANDROID_PLATFORM=${SDK_LEVEL} \
    -DCMAKE_BUILD_TYPE=Release \
    -DENABLE_SHARED=0 \
    -DNEON_ANDROID=1 \
    -DENABLE_CLI=0 \
    -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
    ../source

# 如果DENABLE_CLI=0 不设置或者设置1下面的需要打开
# 只适合 macos linux 还存在问题
#sed -i '' 's/-lpthread/-pthread/' CMakeFiles/cli.dir/link.txt
#sed -i '' 's/-lpthread/-pthread/' CMakeFiles/x265-shared.dir/link.txt
#sed -i '' 's/-lpthread/-pthread/' CMakeFiles/x265-static.dir/link.txt

make -j$(get_cpu_count)
make install/strip


# the last one code
# back to the build script directory
cd ${SCRIPT_DIR}
