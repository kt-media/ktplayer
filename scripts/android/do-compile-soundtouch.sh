#! /usr/bin/env bash
set -e

SCRIPT_NAME=$0
   ROOT_DIR=$1
 SCRIPT_DIR=$2
ANDROID_ABI=$3
TARGET_NAME=$4
 BUILD_SKIP=false
  SDK_LEVEL=18

echo
echo "********** < ${SCRIPT_NAME} > **********"

if [ "$BUILD_SKIP" == "true" ]; then
    exit 0
fi

# load the android common env
source ${SCRIPT_DIR}/tools/env-common.sh
source ${SCRIPT_DIR}/tools/download.sh

if [ "$ANDROID_ABI" == "arm64-v8a" ] || [ "$ANDROID_ABI" == "x86_64" ]; then
    SDK_LEVEL=21
fi

# set link CC CXX AR env
set_android_toolchain ${ANDROID_ABI} ${SDK_LEVEL} ${ROOT_DIR}
# print the base information
## print_android_toolchain_env

# source base information
SRC_ORIG_DIR="$(get_deps_dir ${ROOT_DIR} ${TARGET_NAME})"

# source address
SRC_REMOTE_URL=https://gitlab.com/soundtouch/soundtouch/-/archive/2.1.2/soundtouch-2.1.2.tar.bz2
      SRC_NAME=soundtouch-2.1.2.tar.bz2
      SRC_FILE=${SRC_ORIG_DIR}/${SRC_NAME}

# check the original souce exits
# maybe try to download the original source
download ${SRC_REMOTE_URL} ${SRC_ORIG_DIR} ${SRC_NAME}

# unzip the target source to dest dir
CONTRIB_DIR=${ROOT_DIR}/contrib/${ANDROID_ABI}/${TARGET_NAME}
rm   -rf ${CONTRIB_DIR}
mkdir -p ${CONTRIB_DIR}
tar jxf ${SRC_FILE} --strip-components 1 -C ${CONTRIB_DIR}

# install dir
INSTALL_DIR=$(get_install_dir ${ROOT_DIR} ${ANDROID_ABI})

# copy the CMakeLists.txt to soundtouch directory
if [ ! -f "${CONTRIB_DIR}/CMakeLists.txt" ]; then
    cp ${ROOT_DIR}/scripts/android/cmake/soundtouch/CMakeLists.txt ${CONTRIB_DIR}
fi

# goto target source directory
cd ${CONTRIB_DIR}
mkdir -p android-build-dir
cd       android-build-dir

cmake_dir_bin="$(get_android_cmake_dir)/bin/cmake"
${cmake_dir_bin} -G"Unix Makefiles" \
      -DANDROID_NDK=${ANDROID_NDK} \
      -DANDROID_ABI=${ANDROID_ABI} \
      -DANDROID_PLATFORM=${SDK_LEVEL} \
      -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK}/build/cmake/android.toolchain.cmake \
      -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
      -DCMAKE_BUILD_TYPE=Release \
      ..
make -j$(get_cpu_count)
make install/strip

# the last one code
# back to the build script directory
cd ${SCRIPT_DIR}
