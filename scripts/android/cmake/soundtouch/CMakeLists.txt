cmake_minimum_required(VERSION 3.4.1)

SET(PROJECT_NAME soundtouch)
PROJECT(${PROJECT_NAME})

SET(CMAKE_CXX_FLAGS "-fvisibility=hidden -fdata-sections -ffunction-sections -DANDROID" )

# Compiler configuration:
if (CMAKE_C_COMPILER_ID STREQUAL "GNU" OR CMAKE_C_COMPILER_ID STREQUAL "Clang")
  if (CMAKE_BUILD_TYPE STREQUAL "Release")
    set (CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -s") # strip
  endif ()
endif ()


INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/include)
ADD_LIBRARY(
        ${PROJECT_NAME} SHARED

        source/SoundTouch/AAFilter.cpp
        source/SoundTouch/FIFOSampleBuffer.cpp
        source/SoundTouch/FIRFilter.cpp 
        source/SoundTouch/cpu_detect_x86.cpp
        source/SoundTouch/sse_optimized.cpp
        source/SoundStretch/WavFile.cpp
        source/SoundTouch/RateTransposer.cpp
        source/SoundTouch/SoundTouch.cpp
        source/SoundTouch/InterpolateCubic.cpp
        source/SoundTouch/InterpolateLinear.cpp
        source/SoundTouch/InterpolateShannon.cpp
        source/SoundTouch/TDStretch.cpp
        source/SoundTouch/BPMDetect.cpp 
        source/SoundTouch/PeakFinder.cpp
)


TARGET_LINK_LIBRARIES(
        ${PROJECT_NAME}

        log gcc
)

INSTALL( 
        TARGETS ${PROJECT_NAME}
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
)

FILE(GLOB heads "${CMAKE_SOURCE_DIR}/include/*")
INSTALL(
    FILES ${heads} DESTINATION ${CMAKE_INSTALL_PREFIX}/include
)

