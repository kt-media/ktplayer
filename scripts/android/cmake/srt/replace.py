#!/usr/bin/python

import os
import sys

nargs = len(sys.argv)
if not 1 <= nargs <= 3:
    print('usage: %s search_text repalce_text [infile [outfile]]' % os.path.basename(sys.argv[0]))
else:
    search_text = '''else()
	message(FATAL_ERROR "Unsupported system: ${CMAKE_SYSTEM_NAME}")'''
    replace_text = '''elseif(ANDROID)
    add_definitions(-DLINUX=1)
    add_definitions(-DANDROID=1)
    message(STATUS "DETECTED SYSTEM: ANDROID;  LINUX=1 ANDROID=1")
else()
    message(FATAL_ERROR "Unsupported system: ${CMAKE_SYSTEM_NAME}")'''
    input_file = sys.stdin
    output_file = sys.stdout
    if nargs > 1:
        input_file = open(sys.argv[1], 'r')
    if nargs > 2:
        output_file = open(sys.argv[2], 'w')
    content     = input_file.read()
    new_content = content.replace(search_text, replace_text)
    output_file.write(new_content)
    output_file.close()
    input_file.close()