#!/usr/bin/python

import os
import sys

nargs = len(sys.argv)
if not 1 <= nargs <= 3:
    print('usage: %s search_text repalce_text [infile [outfile]]' % os.path.basename(sys.argv[0]))
else:
    search_text = "SHLIB_EXT=.so.$(SHLIB_VERSION_NUMBER)"
    replace_text = "SHLIB_EXT=.so"
    input_file = sys.stdin
    output_file = sys.stdout
    if nargs > 1:
        input_file = open(sys.argv[1], 'r')
    if nargs > 2:
        output_file = open(sys.argv[2], 'w')
    for s in input_file:
        output_file.write(s.replace(search_text, replace_text))
    output_file.close()
    input_file.close()
