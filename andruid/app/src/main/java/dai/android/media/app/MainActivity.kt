package dai.android.media.app

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import dai.android.media.KtMediaPlayer
import java.io.File

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ActivityCompat.requestPermissions(this, arrayListOf<String>(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        ).toTypedArray(), 1)

        val file = File(getVideoFileExternalPath(this))
        if (!file.exists()) {
            Toast.makeText(this, "can not found video file", Toast.LENGTH_LONG).show()
        }
    }


    fun clickedAudioRecord(view: View) {
    }

    fun clickedCamera(view: View) {
    }

    fun clickedMediaPlay(view: View) {}

    fun clickedExtractor(view: View) {}

    fun clickedMediaCodec(view: View) {
        //val player = KtMediaPlayer(this.application)
        val intent = Intent(this, KtMediaActivity::class.java)
        startActivity(intent)
    }

}
