package dai.android.media.app

import android.content.Context
import android.os.Environment


fun getVideoFileExternalPath(context: Context): String {
    return context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)?.absolutePath + "/dai_demo_kt_video.mp4"
}

fun getVideoExternalPath(context: Context): String {
    return context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)?.absolutePath + "/dai_video_demo"
}