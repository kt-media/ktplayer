package dai.android.media.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import dai.android.media.KtMediaPlayer
import java.util.*

class KtMediaActivity : AppCompatActivity() {
    private var ktPlayer: KtMediaPlayer? = null

    private lateinit var mBtnSetDataSource: Button
    private lateinit var mBtnStart: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kt_media)
        initViews()

        ktPlayer = KtMediaPlayer(application)
    }

    private fun initViews() {
        mBtnSetDataSource = findViewById(R.id.btnSetDataSource)
        mBtnSetDataSource.setOnClickListener(ViewClicked)

        mBtnStart = findViewById(R.id.btnStart)
        mBtnStart.setOnClickListener(ViewClicked)
    }

    private val ViewClicked = View.OnClickListener { v ->
        when (v) {
            mBtnSetDataSource -> {
                ktPlayer?.setDataSource("/sdcard/test/hello.mp4")
            }

            mBtnStart -> {
                ktPlayer?.start()
            }
        }
    }

    override fun onStop() {
        super.onStop()

        ktPlayer?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()

        ktPlayer?.release()
    }
}
