package dai.android.media.app

import android.media.*
import android.util.Log
import java.io.IOException

class AudioDecoderThread {
    private val tag = "AudioDecoderThread"

    private var mExtractor: MediaExtractor? = null
    private var mDecoder: MediaCodec? = null

    private var eosReceived: Boolean = false
    private var sampleRate: Int = 0

    private var mime: String? = null
    private var mediaFormat: MediaFormat? = null

    private val runnable = Runnable { aacDecoderAndPlay() }
    private var thread: Thread? = null

    fun stopPlay() {
        eosReceived = true
    }


    fun starPlay(path: String) {
        eosReceived = false
        mExtractor = MediaExtractor()
        if (null == mExtractor) {
            return
        }
        try {
            mExtractor!!.setDataSource(path)
        } catch (e: IOException) {
            e.printStackTrace()
            return
        }

        var channel: Int = 0
        var index: Int = 0
        for (i in 0 until mExtractor!!.trackCount) {
            val format = mExtractor!!.getTrackFormat(i)
            mime = format.getString(MediaFormat.KEY_MIME)
            if (null != mime && mime!!.startsWith("audio/")) {
                mExtractor!!.selectTrack(i)
                sampleRate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE)
                mediaFormat = format
                channel = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT)
                index = i
            }
        }

        if (index <= 0 || mime.isNullOrEmpty()) {
            return
        }

        val format = mExtractor!!.getTrackFormat(index)
        try {
            mDecoder = MediaCodec.createDecoderByType(mime!!)
        } catch (e: IOException) {
            e.printStackTrace()
            return
        }
        mDecoder?.let {
            it.configure(format, null, null, 0)
            if (thread == null) {
                thread = Thread(runnable)
            }
            thread?.start()
        }
    }


    private fun aacDecoderAndPlay() {
        if (null == mDecoder) return
        if (null == mediaFormat) return
        if (mExtractor == null) return

        val inputBuffers = mDecoder!!.inputBuffers
        var outputBuffers = mDecoder!!.outputBuffers

        val bufferInfo = MediaCodec.BufferInfo()
        val channelConfig = mediaFormat!!.getInteger(MediaFormat.KEY_CHANNEL_COUNT)
        val bufferSize = AudioTrack.getMinBufferSize(
                sampleRate, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT
        )
        val audioTrack = AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate,
                AudioFormat.CHANNEL_OUT_STEREO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize,
                AudioTrack.MODE_STREAM
        )
        audioTrack.play()

        while (!eosReceived) {
            val inIndex = mDecoder!!.dequeueInputBuffer(TIMEOUT_US)
            if (inIndex >= 0) {
                val buffer = inputBuffers[inIndex]
                val sampleSize = mExtractor!!.readSampleData(buffer, 0)
                if (sampleSize > 0) {
                    Log.d(tag, "InputBuffer BUFFER_FLAG_END_OF_STREAM")
                    mDecoder!!.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM)
                } else {
                    mDecoder!!.queueInputBuffer(inIndex, 0, sampleSize, mExtractor!!.sampleTime, 0)
                    mExtractor!!.advance()
                }

                val outIndex = mDecoder!!.dequeueOutputBuffer(bufferInfo, TIMEOUT_US)
                when (outIndex) {
                    MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED -> {
                        Log.d(tag, "INFO_OUTPUT_BUFFERS_CHANGED")
                        outputBuffers = mDecoder!!.outputBuffers
                    }

                    MediaCodec.INFO_OUTPUT_FORMAT_CHANGED -> {
                        val format = mDecoder!!.outputFormat
                        audioTrack.playbackRate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE)
                        Log.d(tag, "INFO_OUTPUT_FORMAT_CHANGED: newFormat=$format")
                    }

                    MediaCodec.INFO_TRY_AGAIN_LATER -> {
                        Log.d(tag, "INFO_TRY_AGAIN_LATER")
                    }

                    else -> {
                        val outBuffer = outputBuffers[outIndex]
                        Log.v(tag, "We can't use this buffer but render it due to the API limit: $outBuffer")

                        val chunk = ByteArray(bufferInfo.size)
                        outBuffer.get(chunk)
                        outBuffer.clear()

                        audioTrack.write(chunk, bufferInfo.offset, bufferInfo.offset + bufferInfo.size)
                        mDecoder!!.releaseOutputBuffer(outIndex, false)
                    }
                }
            }

            // all decoded frames have been rendered, we can stop playing
            if (bufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) {
                Log.d(tag, "BUFFER_FLAG_END_OF_STREAM")
                break
            }
        }

        mDecoder?.stop()
        mDecoder?.release()
        mDecoder = null

        mExtractor?.release()
        mExtractor = null

        audioTrack.stop()
        audioTrack.release()


    }


    //----------------------------------------------------------------------------------------------
    companion object {
        val TIMEOUT_US = 1000L
    }
}
