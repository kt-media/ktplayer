package dai.android.media

import android.os.*
import android.view.Surface
import dai.android.media.service.IKtMediaPlayerClient
import java.lang.ref.WeakReference


fun sendMessage(handler: Handler, what: Int) {
    handler.obtainMessage(what).sendToTarget()
}

fun sendMessage(handler: Handler, what: Int, any: Any?) {
    handler.obtainMessage(what, any).sendToTarget()
}

fun sendMessage(handler: Handler, what: Int, arg1: Int, arg2: Int) {
    handler.obtainMessage(what, arg1, arg2).sendToTarget()
}

fun sendMessage(handler: Handler, what: Int, arg1: Int, arg2: Int, any: Any?) {
    handler.obtainMessage(what, arg1, arg2, any).sendToTarget()
}

//==================================================================================================

// command code
internal const val CMD_CONNECTION_CHECK = 0x00
internal const val CMD_ATTACH_CLIENT = 0x11

internal const val CMD_SET_DATA_SOURCE_BY_STR = 0x21
internal const val CMD_SET_DATA_SOURCE_BY_FD = 0x22
internal const val CMD_SET_DATA_SOURCE_BY_KT = 0x23
internal const val CMD_SET_SURFACE = 0x2F

internal const val CMD_START = 0x31
internal const val CMD_PAUSE = 0x41
internal const val CMD_STOP = 0x51
internal const val CMD_RELEASE = 0x61
internal const val CMD_RESET = 0x62
internal const val CMD_SEEK_TO = 0x71
internal const val CMD_SET_VOLUME = 0x81
internal const val CMD_PREPARE_ASYNC = 0x91


// event code
internal const val EVENT_MEDIA_PREPARED = 0x11
internal const val EVENT_MEDIA_PLAYBACK_COMPLETE = 0x21
internal const val EVENT_MEDIA_BUFFERING_UPDATE = 0x31
internal const val EVENT_MEDIA_SEEK_COMPLETE = 0x41
internal const val EVENT_MEDIA_VIDEO_SIZE_SET = 0x51
internal const val EVENT_MEDIA_VIDEO_SAR_SET = 0x52
internal const val EVENT_MEDIA_INFO = 0x61
internal const val EVENT_MEDIA_ERROR = 0x71
internal const val EVENT_MEDIA_PLAYER_CLOCK_CHANGE = 0x81

//==================================================================================================


internal class KtWorkHandler(val player: KtMediaPlayer, looper: Looper) : Handler(looper) {
    private val workerMessageQueue = arrayListOf<Message>()

    fun pushToMessageQueue(what: Int) {
        synchronized(workerMessageQueue) {
            workerMessageQueue.add(obtainMessage(what))
        }
    }

    fun sendOrPushToMessageQueue(push: Boolean, what: Int) {
        sendOrPushToMessageQueue(push, obtainMessage(what))
    }

    fun pushToMessageQueue(what: Int, any: Any?) {
        synchronized(workerMessageQueue) {
            workerMessageQueue.add(obtainMessage(what, any))
        }
    }

    fun sendOrPushToMessageQueue(push: Boolean, what: Int, any: Any?) {
        sendOrPushToMessageQueue(push, obtainMessage(what, any))
    }

    fun pushToMessageQueue(what: Int, arg1: Int, arg2: Int) {
        synchronized(workerMessageQueue) {
            workerMessageQueue.add(obtainMessage(what, arg1, arg2))
        }
    }

    fun sendOrPushToMessageQueue(push: Boolean, what: Int, arg1: Int, arg2: Int) {
        sendOrPushToMessageQueue(push, obtainMessage(what, arg1, arg2))
    }

    fun pushToMessageQueue(what: Int, arg1: Int, arg2: Int, any: Any) {
        synchronized(workerMessageQueue) {
            workerMessageQueue.add(obtainMessage(what, arg1, arg2, any))
        }
    }

    private fun sendOrPushToMessageQueue(push: Boolean, message: Message) {
        if (!push) {
            sendMessage(message)
        } else {
            synchronized(workerMessageQueue) {
                if (!push) {
                    sendMessage(message)
                } else {
                    workerMessageQueue.add(message)
                }
            }
        }
    }

    override fun handleMessage(message: Message) {
        when (message.what) {
            CMD_CONNECTION_CHECK -> doConnectionCheck()

            CMD_ATTACH_CLIENT -> doAttachClient()

            CMD_RELEASE -> doRelease()

            CMD_SET_SURFACE -> doSetSurface(message)

            CMD_PREPARE_ASYNC -> doPrepareAsync()

            CMD_START -> doStart()

            CMD_PAUSE -> doPause()

            CMD_STOP -> doStop()

            CMD_RESET -> doReset()

            CMD_SEEK_TO -> doSeekTo(message)

            CMD_SET_DATA_SOURCE_BY_STR -> doSetDataSourceStr(message)
            CMD_SET_DATA_SOURCE_BY_FD -> doSetDataSourceFd(message)
            CMD_SET_DATA_SOURCE_BY_KT -> doSetDataSourceKt(message)
        }
    }

    private fun doConnectionCheck() {
        if (isServiceConnected) {
            removeCallbacksAndMessages(null)
            sendMessage(this, CMD_ATTACH_CLIENT)
        } else {
            sendEmptyMessageDelayed(CMD_CONNECTION_CHECK, 15)
        }
    }


    private fun doSetDataSourceKt(message: Message) {
        if (message.obj is KtDataSource) {
            player.setDataSourceKt(message.obj as KtDataSource)
        }
    }

    private fun doSetDataSourceFd(message: Message) {
        if (message.obj is ParcelFileDescriptor) {
            player.setDataSourceFd(message.obj as ParcelFileDescriptor)
        }
    }

    private fun doSetDataSourceStr(message: Message) {
        if (message.obj is String) {
            player.setDataSourceStr(message.obj as String)
        }
    }

    private fun doSeekTo(message: Message) {
        player.seekToInner(message.arg1.toLong(), message.arg2)
    }

    private fun doReset() {
        player.resetInner()
    }

    private fun doStop() {
        player.stopInner()
    }

    private fun doPause() {
        player.pauseInner()
    }

    private fun doStart() {
        player.startInner()
    }

    private fun doPrepareAsync() {
        player.prepareAsyncInner()
    }

    private fun doSetSurface(message: Message) {
        if (message.obj is Surface) {
            player.setSurfaceInner(message.obj as Surface)
        }
    }

    internal fun clean() {
        removeCallbacksAndMessages(null)
        synchronized(workerMessageQueue) {
            workerMessageQueue.clear()
        }
    }

    private fun doRelease() {
        clean()
        player.releaseInner()
    }

    private fun doAttachClient() {
        if (null != player.clientCallback) {
            var client: IKtMediaPlayerClient? = null
            try {
                client = serviceConnection.attachPlayerToService(player.getClientCode(), player.clientCallback!!, player)
            } catch (e: Exception) {
                // todo log
            }

            if (null == client) {
                stopMediaService(player.context)
            }

            synchronized(workerMessageQueue) {
                workerMessageQueue.forEach { action ->
                    sendMessage(action)
                }
                workerMessageQueue.clear()

                player.mediaClient = client
            }

            if (null == player.mediaClient) {
                removeCallbacksAndMessages(null)
            }
        }
    }
}

internal class KtMediaEventHandler(
        private val player: KtMediaPlayer,
        looper: Looper
) : Handler(looper) {
    private val playerRef = WeakReference(player)

    private fun getPlayer(): KtMediaPlayer? {
        return playerRef.get()
    }

    override fun handleMessage(message: Message) {
        when (message.what) {

            EVENT_MEDIA_PREPARED -> doEventPrepared()

            EVENT_MEDIA_BUFFERING_UPDATE -> doEventBufferingUpdate(message)

            EVENT_MEDIA_SEEK_COMPLETE -> doEventSeekComplete()

            EVENT_MEDIA_VIDEO_SIZE_SET -> doEventVideoSizeSet(message)

            EVENT_MEDIA_VIDEO_SAR_SET -> doEventVideoSarSet(message)

            EVENT_MEDIA_ERROR -> doEventOnError(message)

            EVENT_MEDIA_INFO -> doEventOnInfo(message)
        }
    }

    private fun doEventOnInfo(message: Message) {
        getPlayer()?.apply {
            val bundle = if (message.obj is Bundle) message.obj as Bundle else null
            notifyOnInfoInner(message.arg1, message.arg2, bundle)
        }
    }

    private fun doEventOnError(message: Message) {
        getPlayer()?.apply {
            notifyOnErrorInner(message.arg1, message.arg2)
        }
    }

    private fun doEventVideoSarSet(message: Message) {
        getPlayer()?.apply {
            mVideoSarNum = message.arg1
            mVideoSarDen = message.arg2

            notifyOnVideoSizeChanged(mVideoWidth, mVideoHeight, mVideoSarNum, mVideoSarDen)
        }
    }

    private fun doEventVideoSizeSet(message: Message) {
        if (message.obj is Bundle) {
            getPlayer()?.apply {
                val args = message.obj as Bundle
                mVideoWidth = args.getInt("width", 0)
                mVideoHeight = args.getInt("height", 0)
                mVideoSarNum = args.getInt("sar_num", 0)
                mVideoSarDen = args.getInt("sar_den", 1)

                notifyOnVideoSizeChanged(mVideoWidth, mVideoHeight, mVideoSarNum, mVideoSarDen)
            }
        }
    }


    private fun doEventSeekComplete() {
        getPlayer()?.apply {
            notifyOnSeekComplete()
        }
    }

    private fun doEventBufferingUpdate(message: Message) {
        getPlayer()?.apply {
            val bufferPosition = if (message.arg1 <= 0) 0 else message.arg1

            var percent = 0L
            val duration = getDuration()
            if (duration > 0) {
                percent = bufferPosition * 100 / duration
            }
            if (percent >= 100) percent = 100L;

            notifyOnBufferingUpdate(percent.toInt())
        }
    }

    private fun doEventPrepared() {
        getPlayer()?.notifyOnPrepared()
    }
}
