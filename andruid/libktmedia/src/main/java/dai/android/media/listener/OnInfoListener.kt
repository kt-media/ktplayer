package dai.android.media.listener

import android.os.Bundle
import dai.android.media.AbstractMediaPlayer

interface OnInfoListener {
    fun onInfo(player: AbstractMediaPlayer, what: Int, extra: Int, extraData: Bundle?): Boolean
}
