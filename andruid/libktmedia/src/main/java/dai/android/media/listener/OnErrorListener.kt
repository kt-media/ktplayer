package dai.android.media.listener

import dai.android.media.AbstractMediaPlayer

interface OnErrorListener {
    fun onError(player: AbstractMediaPlayer, what: Int, extra: Int): Boolean
}
