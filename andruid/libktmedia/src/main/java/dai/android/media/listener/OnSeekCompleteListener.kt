package dai.android.media.listener

import dai.android.media.AbstractMediaPlayer

interface OnSeekCompleteListener {
    fun onSeekComplete(player: AbstractMediaPlayer)
}
