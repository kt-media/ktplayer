package dai.android.media.listener

import dai.android.media.AbstractMediaPlayer

interface OnPreparedListener {
    fun onPrepared(player: AbstractMediaPlayer)
}
