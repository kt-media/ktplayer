package dai.android.media.listener

interface OnServiceConnectionListener {

    fun onConnected()

    fun onDisconnected();

}
