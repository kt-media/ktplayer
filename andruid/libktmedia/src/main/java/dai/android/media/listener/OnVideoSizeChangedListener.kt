package dai.android.media.listener

import dai.android.media.AbstractMediaPlayer

interface OnVideoSizeChangedListener {
    fun onVideoSizeChanged(player: AbstractMediaPlayer, width: Int, height: Int, sar_num: Int, sar_den: Int)
}
