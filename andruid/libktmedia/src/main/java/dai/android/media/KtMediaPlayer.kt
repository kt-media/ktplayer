package dai.android.media

import android.annotation.SuppressLint
import android.app.Application
import android.content.ContentResolver
import android.content.Context
import android.content.res.AssetFileDescriptor
import android.media.RingtoneManager
import android.net.Uri
import android.os.*
import android.os.PowerManager.WakeLock
import android.provider.Settings
import android.view.Surface
import android.view.SurfaceHolder
import dai.android.media.service.IKtMediaPlayerClient
import dai.android.media.service.KtMediaPlayerClientCallback
import java.io.FileDescriptor
import java.io.FileNotFoundException
import java.io.IOException
import java.util.concurrent.atomic.AtomicInteger

class KtMediaPlayer(
        internal val context: Application,
        internal val loader: ILibraryLoader?,
        internal val parameter: KtParameter?,
        internal val mediaType: KtMediaType = MEDIA_FF_PLAYER
) : AbstractMediaPlayer() {

    private val TAG: String = "KtMediaPlayer"

    constructor(context: Application, loader: ILibraryLoader?) : this(context, loader, null)
    constructor(context: Application) : this(context, null)


    private val mWorkThread = HandlerThread("KtMediaPlayerThread")
    private var WH: KtWorkHandler
    private var EH: KtMediaEventHandler? = null

    private val mClientCode = obtainClientCode()
    private var mReleased = false

    private var mDisplaySurface: Surface? = null
    private var mSurfaceHolder: SurfaceHolder? = null
    private var mKeepScreenOn: Boolean = false
    private var mStayAwake: Boolean = false
    private var mWakeLock: WakeLock? = null

    internal var mDuration: Long = -1L
    internal var mVideoWidth: Int = 0
    internal var mVideoHeight: Int = 0
    internal var mVideoSarNum: Int = 0
    internal var mVideoSarDen: Int = 1


    internal var clientCallback: KtMediaPlayerClientCallback? = null
    internal var mediaClient: IKtMediaPlayerClient? = null

    init {
        mWorkThread.start()
        WH = KtWorkHandler(this, mWorkThread.looper)

        clientCallback = KtMediaPlayerClientCallback(this)

        // event looper
        // get current thread looper first OR
        // use main thread
        val eventLooper = Looper.myLooper() ?: Looper.myLooper()
        eventLooper?.let {
            EH = KtMediaEventHandler(this, eventLooper)
        }
        //// EH = KtMediaEventHandler(this, mWorkThread.looper)

        // now start service
        startMediaService(context, loader)
        sendMessage(WH, CMD_CONNECTION_CHECK)
    }


    override fun getClientCode(): Int {
        return mClientCode
    }


    @Throws(Exception::class)
    override
    fun setDataSource(context: Context, uri: Uri) {
        if (mReleased) return

        val scheme = uri.scheme
        if (ContentResolver.SCHEME_FILE == scheme) {
            setDataSource(uri.path!!)
            return
        }

        var newUri: Uri = uri
        if (ContentResolver.SCHEME_CONTENT == scheme && Settings.AUTHORITY == uri.authority) {
            // Redirect ringtones to go directly to underlying provider
            newUri = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.getDefaultType(uri))
            if (newUri == null) {
                throw FileNotFoundException("Failed to resolve default ringtone")
            }
        }

        var fd: AssetFileDescriptor? = null
        try {
            val resolver = context.contentResolver
            fd = resolver.openAssetFileDescriptor(uri, "r")
            if (fd == null) {
                return
            }
            // Note: using getDeclaredLength so that our behavior is the same
            // as previous versions when the content provider is returning
            // a full file.
            if (fd.declaredLength < 0) {
                setDataSource(fd.fileDescriptor)
            } else {
                // setDataSource(fd.fileDescriptor, fd.startOffset, fd.declaredLength)
                setDataSource(fd.fileDescriptor)
            }
            return
        } catch (ignored: SecurityException) {
        } catch (ignored: IOException) {
        } finally {
            fd?.close()
        }

        setDataSource(newUri.toString())
    }

    override fun setDataSource(fd: FileDescriptor) {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_SET_DATA_SOURCE_BY_FD, ParcelFileDescriptor.dup(fd))
    }

    override fun setDataSource(path: String) {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_SET_DATA_SOURCE_BY_STR, path)
    }

    override fun setDataSource(dataSource: KtDataSource) {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_SET_DATA_SOURCE_BY_KT, dataSource)
    }

    override fun prepareAsync() {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_PREPARE_ASYNC)
    }

    override fun start() {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_START)
    }

    override fun stop() {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_STOP)
    }

    override fun pause() {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_PAUSE)
    }

    override fun reset() {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_RESET)
    }

    override fun release() {
        if (mReleased) return
        mReleased = true

        stayAwake(false)

        // 同步释放
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            releaseInner()
            WH.clean()
        } else {
            sendMessage(WH, CMD_RELEASE)
        }
        try {
            mWorkThread.join()
        } catch (ignore: Exception) {
        }
        mediaClient = null
        clientCallback = null
        resetListeners()
    }

    override fun seekTo(msec: Long) = seekTo(msec, false)

    fun seekTo(msec: Long, accurate: Boolean) = seekTo(msec, accurate, false)

    fun seekTo(msec: Long, accurate: Boolean, async: Boolean) {
        if (mReleased) return

        val mode = if (accurate) 1 else 0
        if (async) {
            WH.sendOrPushToMessageQueue(!isClientWork(), CMD_SEEK_TO, mode)
        } else {
            if (isClientWork()) {
                seekToInner(msec, mode)
            } else {
                seekTo(msec, accurate, true)
            }
        }
    }

    override fun setSurface(surface: Surface) {
        if (mReleased) return

        // set Surface function keepScreenOnWhilePlaying not work
        if (mKeepScreenOn) { // for log
        }

        mSurfaceHolder = null
        mDisplaySurface = null

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_SET_SURFACE, surface)
    }

    override fun setDisplay(holder: SurfaceHolder) {
        if (mReleased) return

        mSurfaceHolder = holder
        mDisplaySurface = null
        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_SET_SURFACE, mDisplaySurface)
    }

    override fun setWakeMode(context: Context, mode: Int) {
        var wasHeld = false
        if (mWakeLock != null) {
            if (mWakeLock!!.isHeld) {
                wasHeld = true
                mWakeLock!!.release()
            }
            mWakeLock = null
        }
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        mWakeLock = pm.newWakeLock(mode or PowerManager.ON_AFTER_RELEASE, KtMediaPlayer::class.java.name)
        mWakeLock?.setReferenceCounted(false)
        if (wasHeld) {
            mWakeLock?.acquire()
        }
    }

    override fun keepScreenOnWhilePlaying(on: Boolean) {
        if (mKeepScreenOn != on) {
            if (on && mSurfaceHolder != null) {
            }
            mKeepScreenOn = on
            updateSurfaceScreenOn()
        }
    }

    override fun getDuration(): Long {
        if (mReleased) return -1L
        return mDuration
    }

    override fun getCurrentPosition(): Long {
        if (mReleased) return 0L

        if (isClientWork()) {
            try {
                return mediaClient!!.currentPosition
            } catch (e: Exception) {
            }
        }
        return 0L
    }

    override fun setVolume(left: Float, right: Float) {
        if (mReleased) return

        WH.sendOrPushToMessageQueue(!isClientWork(), CMD_SET_VOLUME, Pair(left, right))
    }

    internal fun setVolumeInner(left: Float, right: Float) {
        if (isClientWork()) {
            try {
                mediaClient!!.setVolume(left, right)
            } catch (e: Exception) {
            }
        }
    }

    internal fun setDataSourceKt(ds: KtDataSource) {
        if (isClientWork()) {
            try {
                mediaClient!!.setDataSourceKt(ds)
            } catch (e: Exception) {
            }
        }
    }

    internal fun setDataSourceStr(path: String) {
        if (isClientWork()) {
            try {
                mediaClient!!.setDataSourceStr(path)
            } catch (e: Exception) {
            }
        }
    }

    internal fun setDataSourceFd(fd: ParcelFileDescriptor) {
        if (isClientWork()) {
            try {
                mediaClient!!.setDataSourceFd(fd)
            } catch (e: Exception) {
            }
        }
    }


    internal fun setSurfaceInner(surface: Surface) {
        if (isClientWork()) {
            mDisplaySurface = surface
            mediaClient!!.setSurface(surface)
            updateSurfaceScreenOn()
        }
    }

    internal fun seekToInner(msec: Long, mode: Int) {
        if (isClientWork()) {
            try {
                mediaClient!!.seekTo(msec, mode)
            } catch (e: Exception) {
            }
        }
    }

    internal fun resetInner() {
        if (isClientWork()) {
            mediaClient!!.reset()
            stayAwake(false)
        }
        // reset another ..
    }

    internal fun stopInner() {
        if (isClientWork()) {
            mediaClient!!.stop()
            stayAwake(false)
        }
    }

    internal fun pauseInner() {
        if (isClientWork()) {
            try {
                mediaClient!!.pause()
                stayAwake(false)
            } catch (e: Exception) {
            }
        }
    }

    internal fun startInner() {
        if (isClientWork()) {
            try {
                mediaClient!!.start()
            } catch (e: Exception) {
            }

            stayAwake(true)
        }
    }

    internal fun prepareAsyncInner() {
        if (isClientWork()) {
            try {
                mediaClient!!.prepareAsync()
            } catch (e: Exception) {
            }
        }
    }

    internal fun releaseInner() {
        if (isClientWork()) {
            try {
                mediaClient?.setSurface(null)
                mediaClient?.pause()
                mediaClient?.release()
            } catch (e: Exception) {
            }
        }
        try {
            serviceConnection.detachPlayerFromServer(getClientCode())
        } catch (e: Exception) {
        }
        mWorkThread.quitSafely()
    }

    internal fun handleConnected() {
        mOnServiceConnection?.apply {
            onConnected()
        }
    }

    internal fun handleDisconnected() {
        mOnServiceConnection?.apply {
            onDisconnected()
        }
    }

    internal fun notifyOnErrorInner(what: Int, extra: Int) {
        if (!notifyOnError(what, extra)) {
            notifyOnCompletion()
        }
        stayAwake(false)
    }

    internal fun notifyOnInfoInner(what: Int, extra: Int, args: Bundle?) {
        notifyOnInfo(what, extra, args)
    }

    private fun isClientWork(): Boolean {
        return null != mediaClient && isServiceConnected
    }

    private fun updateSurfaceScreenOn() {
        mSurfaceHolder?.apply {
            setKeepScreenOn(mKeepScreenOn && mStayAwake)
        }
    }

    @SuppressLint("Wakelock")
    private fun stayAwake(awake: Boolean) {
        if (mWakeLock != null) {
            if (awake && !mWakeLock!!.isHeld) {
                mWakeLock!!.acquire()
            } else if (!awake && mWakeLock!!.isHeld) {
                mWakeLock!!.release()
            }
        }
        mStayAwake = awake
        updateSurfaceScreenOn()
    }


    //----------------------------------------------------------------------------------------------
    // companion
    private companion object {

        private val playerClientCode = AtomicInteger(0)

        fun obtainClientCode(): Int {
            playerClientCode.compareAndSet(Int.MAX_VALUE, 0);
            return playerClientCode.getAndIncrement()
        }

    }
}
