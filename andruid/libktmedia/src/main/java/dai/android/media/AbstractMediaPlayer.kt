package dai.android.media

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.Surface
import android.view.SurfaceHolder
import dai.android.media.listener.*
import java.io.FileDescriptor

abstract class AbstractMediaPlayer {

    private var mOnPrepared: OnPreparedListener? = null
    private var mOnCompletion: OnCompletionListener? = null
    private var mOnBuffering: OnBufferingUpdateListener? = null
    private var mOnSeekComplete: OnSeekCompleteListener? = null
    private var mOnVideoSizeChanged: OnVideoSizeChangedListener? = null
    private var mOnError: OnErrorListener? = null
    private var mOnInfo: OnInfoListener? = null
    protected var mOnServiceConnection: OnServiceConnectionListener? = null

    protected fun resetListeners() {
        mOnPrepared = null
        mOnCompletion = null
        mOnBuffering = null
        mOnSeekComplete = null
        mOnVideoSizeChanged = null
        mOnError = null
        mOnInfo = null
        mOnServiceConnection = null
    }

    fun setOnServiceConnection(listener: OnServiceConnectionListener?) {
        mOnServiceConnection = listener
    }

    fun setOnPrepared(listener: OnPreparedListener?) {
        mOnPrepared = listener
    }

    fun setOnCompletion(listener: OnCompletionListener?) {
        mOnCompletion = listener
    }

    fun setOnBuffering(listener: OnBufferingUpdateListener?) {
        mOnBuffering = listener
    }

    fun setOnSeekComplete(listener: OnSeekCompleteListener?) {
        mOnSeekComplete = listener
    }

    fun setOnVideoSizeChanged(listener: OnVideoSizeChangedListener?) {
        mOnVideoSizeChanged = listener
    }

    fun setOnError(listener: OnErrorListener?) {
        mOnError = listener
    }

    fun setOnInfo(listener: OnInfoListener?) {
        mOnInfo = listener
    }

    internal fun notifyOnPrepared() {
        mOnPrepared?.onPrepared(this)
    }

    protected fun notifyOnCompletion() {
        mOnCompletion?.onCompletion(this)
    }

    internal fun notifyOnBufferingUpdate(percent: Int) {
        mOnBuffering?.onBufferingUpdate(this, percent)
    }

    internal fun notifyOnSeekComplete() {
        mOnSeekComplete?.onSeekComplete(this)
    }

    internal fun notifyOnVideoSizeChanged(width: Int, height: Int, sarNum: Int, sarDen: Int) {
        mOnVideoSizeChanged?.onVideoSizeChanged(this, width, height, sarNum, sarDen)
    }

    protected fun notifyOnError(what: Int, extra: Int): Boolean {
        return mOnError != null && mOnError!!.onError(this, what, extra)
    }

    protected fun notifyOnInfo(what: Int, extra: Int, args: Bundle?) {
        mOnInfo?.onInfo(this, what, extra, args)
    }

    abstract fun getClientCode(): Int;

    abstract fun setDataSource(fd: FileDescriptor)
    abstract fun setDataSource(dataSource: KtDataSource)
    abstract fun setDataSource(context: Context, uri: Uri);
    abstract fun setDataSource(path: String)

    abstract fun prepareAsync()

    abstract fun start()

    abstract fun stop()

    abstract fun pause()

    abstract fun reset();

    abstract fun release();

    abstract fun seekTo(msec: Long)

    abstract fun setSurface(surface: Surface)

    abstract fun setDisplay(holder: SurfaceHolder)

    abstract fun getDuration(): Long

    abstract fun getCurrentPosition(): Long

    abstract fun setVolume(left: Float, right: Float)

    abstract fun setWakeMode(context: Context, mode: Int);

    abstract fun keepScreenOnWhilePlaying(on: Boolean);
}
