package dai.android.media.annotations

@Target(AnnotationTarget.FIELD)
@kotlin.annotation.Retention(AnnotationRetention.BINARY)
annotation class NativeAccess