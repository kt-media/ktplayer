package dai.android.media.service

import android.os.ParcelFileDescriptor
import android.text.TextUtils
import android.util.Log
import android.view.Surface
import dai.android.media.KtDataSource
import dai.android.media.KtMediaType
import dai.android.media.MEDIA_FF_PLAYER
import dai.android.media.PlayerDeathBinder
import dai.android.media.annotations.NativeAccess
import java.lang.annotation.Native

internal class KtMediaPlayerClient(
        private val cb: IKtMediaPlayerClientCallback,
        private val type: KtMediaType = MEDIA_FF_PLAYER
) : IKtMediaPlayerClient.Stub() {
    private val tag = "KtMediaPlayerClient"

    internal var isClientBlocked = false
    private var mSurface: Surface? = null

    init {
        native_setup(this, type = type.getMediaType())

        Log.d(tag, "[init]: Java=0x${getHashCodeString()} Native=0x${getObjectPtrString()} ")
    }

    private fun getObjectPtrString(): String {
        return mNativePlayerClient.toString(16)
    }

    private fun getHashCodeString(): String {
        return hashCode().toString(16)
    }

    override fun setDataSourceFd(fd: ParcelFileDescriptor?) {
        if (null == fd) return
        try {
            native_setDataSourceFd(fd.fd)
        } catch (e: Exception) {
        }
    }

    override fun setDataSourceKt(ds: KtDataSource?) {
        if (null == ds) return

        try {
            native_setDataSourceKt(ds)
        } catch (e: Exception) {
        }
    }

    override fun setDataSourceStr(path: String?) {
        if (TextUtils.isEmpty(path)) return

        try {
            native_setDataSourceStr(path!!)
        } catch (e: Exception) {
        }
    }

    override fun getDuration(): Long {
        try {
            return native_getDuration()
        } catch (e: Exception) {
        }
        return 0L
    }

    override fun seekTo(msec: Long, mode: Int) {
        try {
            native_seekTo(msec, mode)
        } catch (e: Exception) {
        }
    }

    override fun getCurrentPosition(): Long {
        try {
            return native_getCurrentPosition()
        } catch (e: Exception) {
        }
        return 0L
    }


    override fun start() {
        try {
            native_start()
        } catch (e: Exception) {
        }
    }

    override fun setVolume(left: Float, right: Float) {
        try {
            native_setVolume(left, right)
        } catch (e: Exception) {
        }
    }


    override fun reset() {
        try {
            native_reset()
        } catch (e: Exception) {
        }
    }

    override fun pause() {
        try {
            native_pause()
        } catch (e: Exception) {
        }
    }

    override fun prepareAsync() {
        try {
            native_prepareAsync()
        } catch (e: Exception) {
        }
    }

    override fun setSurface(surface: Surface?) {
        surface?.let {
            try {
                native_setVideoSurface(it)
                mSurface?.release()
                mSurface = it
            } catch (e: Exception) {
            }
        }
    }

    override fun stop() {
        try {
            native_stop()
        } catch (e: Exception) {
        }
    }


    override fun release() {
        try {
            native_release()
        } catch (e: Exception) {
        }

        mSurface?.apply { release() }
        mSurface = null
    }


    fun attachDeath(binder: PlayerDeathBinder) {}

    fun detachDeath() {}

    fun actionClientDeath() {}

    fun quitHandleThread() {}


    //----------------------------------------------------------------------------------------------
    // native function
    //


    @NativeAccess
    private var mNativePlayerClient: Long = 0L

    private external fun native_setup(any: Any, type: Int)

    private external fun native_finalize()

    private external fun native_setDataSourceFd(fd: Int)

    private external fun native_setDataSourceStr(path: String)

    private external fun native_setDataSourceKt(source: KtDataSource)

    private external fun native_prepareAsync()

    private external fun native_start()

    private external fun native_stop()

    private external fun native_pause()

    private external fun native_seekTo(msec: Long, flag: Int)

    private external fun native_release()

    private external fun native_reset()

    private external fun native_getDuration(): Long

    private external fun native_getCurrentPosition(): Long

    private external fun native_setVolume(left: Float, right: Float)

    private external fun native_setVideoSurface(surface: Surface)
}
