package dai.android.media.listener

import dai.android.media.AbstractMediaPlayer

interface OnBufferingUpdateListener {
    fun onBufferingUpdate(player: AbstractMediaPlayer, percent: Int)
}
