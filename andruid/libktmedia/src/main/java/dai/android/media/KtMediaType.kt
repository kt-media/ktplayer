package dai.android.media

import android.os.Parcel
import android.os.Parcelable

class KtMediaType(private val value: Int) : Parcelable {

    fun getMediaType(): Int {
        return value
    }

    constructor(parcel: Parcel) : this(parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KtMediaType> {
        override fun createFromParcel(parcel: Parcel): KtMediaType {
            return KtMediaType(parcel)
        }

        override fun newArray(size: Int): Array<KtMediaType?> {
            return arrayOfNulls(size)
        }
    }
}


/**
 * FF_PLAYER
 */
val MEDIA_FF_PLAYER = KtMediaType(1)

/**
 * GST_PLAYER
 */
val MEDIA_GST_PLAYER = KtMediaType(2)
