package dai.android.media

import android.os.Bundle
import java.io.File

/**
 * the interface library loader
 */
interface ILibraryLoader {

    @Throws(UnsatisfiedLinkError::class, SecurityException::class)
    fun loadLibrary(name: String)

    fun findLibrary(name: String): File?
}

private val localLibraryLoader = object : ILibraryLoader {
    override fun loadLibrary(name: String) {
        System.loadLibrary(name)
    }

    override fun findLibrary(name: String): File? {
        return null
    }
}

private var isLibraryLoaded = false
internal fun loadLibrariesOnce(bundle: Bundle?) {
    if (isLibraryLoaded) return

    if (null != bundle) {
        System.load(".......")
    } else {
        // ktmedia crypto ssl soxr srt srtp2 yuv soundtouch ffmpeg
        localLibraryLoader.loadLibrary("ktmedia")
        localLibraryLoader.loadLibrary("crypto")
        localLibraryLoader.loadLibrary("ssl")
        localLibraryLoader.loadLibrary("soxr")
        localLibraryLoader.loadLibrary("srt")
        localLibraryLoader.loadLibrary("srtp2")
        localLibraryLoader.loadLibrary("yuv")
        localLibraryLoader.loadLibrary("soundtouch")
        localLibraryLoader.loadLibrary("ffmpeg")
    }

    isLibraryLoaded = true
}
