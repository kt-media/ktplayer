package dai.android.media.service

import dai.android.media.*
import dai.android.media.PlayerDeathBinder
import dai.android.media.addMediaPlayerClient
import dai.android.media.getMediaPlayerClient
import dai.android.media.removeMediaPlayerClient
import java.lang.ref.WeakReference

internal class KtMediaServiceStub(service: KtMediaService) : IKtMediaPlayerService.Stub() {
    private val serviceRef = WeakReference(service)

    private fun getService(): KtMediaService? {
        return serviceRef.get()
    }

    override
    fun attachMediaPlayerClient(id: Int, cb: IKtMediaPlayerClientCallback, type: KtMediaType): IKtMediaPlayerClient? {
        getService()?.apply {
            val client = KtMediaPlayerClient(cb, type)
            client.attachDeath(PlayerDeathBinder(id, this))
            addMediaPlayerClient(id, client)
            return client
        }
        return null
    }


    override fun detachMediaPlayerClient(id: Int) {
        getService()?.apply {
            val client = getMediaPlayerClient(id)
            client?.let {
                if (it is KtMediaPlayerClient) {
                    it.detachDeath()
                    it.quitHandleThread()
                }
            }
            removeMediaPlayerClient(id)
        }
        scanBlockAndExit()
    }
}
