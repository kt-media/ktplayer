package dai.android.media.listener

import dai.android.media.AbstractMediaPlayer

interface OnCompletionListener {
    fun onCompletion(player: AbstractMediaPlayer)
}
