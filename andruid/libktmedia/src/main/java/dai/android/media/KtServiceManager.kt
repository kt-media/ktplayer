package dai.android.media

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.AndroidException
import android.util.SparseArray
import androidx.core.util.forEach
import dai.android.media.service.*
import java.lang.ref.WeakReference
import kotlin.system.exitProcess

// 是否已经连接了 KtMediaPlayerService
@Volatile
internal var isServiceConnected: Boolean = false

internal val serviceConnection = MediaServiceConnection()

internal class MediaServiceConnection : ServiceConnection {
    private var mServiceInterface: IKtMediaPlayerService? = null

    private val playerClients = SparseArray<WeakReference<KtMediaPlayer>>()

    @Throws(AndroidException::class)
    fun attachPlayerToService(
            id: Int,
            cb: IKtMediaPlayerClientCallback,
            player: KtMediaPlayer
    ): IKtMediaPlayerClient {
        synchronized(this) {
            if (null == mServiceInterface) {
                throw AndroidException("KtMediaService not connected while attach to service")
            }

            try {
                val _client = mServiceInterface!!.attachMediaPlayerClient(id, cb, player.mediaType)
                playerClients.put(id, WeakReference(player))
                return _client
            } catch (e: Exception) {
                throw AndroidException("Can't attach client[$id] to service")
            }
        }
    }

    @Throws(AndroidException::class)
    fun detachPlayerFromServer(id: Int) {
        synchronized(this) {
            if (null == mServiceInterface) {
                throw AndroidException("KtMediaService not connected while detach from service")
            }

            try {
                mServiceInterface!!.detachMediaPlayerClient(id)
                playerClients.remove(id)
            } catch (e: Exception) {
                throw AndroidException("Can't detach client[$id] from service")
            }
        }
    }


    override fun onServiceDisconnected(name: ComponentName?) {
        synchronized(this) {
            isServiceConnected = false

            playerClients.forEach { _, value ->
                val player = value.get()
                player?.apply {
                    handleDisconnected()
                }
            }
            // clean all KtMediaPlayer
            playerClients.clear()

            mServiceInterface = null
        }
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        if (null == service) return
        synchronized(this) {
            mServiceInterface = IKtMediaPlayerService.Stub.asInterface(service)
            isServiceConnected = true

            playerClients.forEach { _, value ->
                val player = value.get()
                player?.apply {
                    handleConnected()
                }
            }
        }
    }
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// clients
//
private val mediaPlayerClientRefs = SparseArray<WeakReference<IKtMediaPlayerClient>>()

internal fun getMediaPlayerClient(id: Int): IKtMediaPlayerClient? {
    synchronized(mediaPlayerClientRefs) {
        return mediaPlayerClientRefs.get(id)?.get()
    }
}

internal fun addMediaPlayerClient(id: Int, client: IKtMediaPlayerClient) {
    synchronized(mediaPlayerClientRefs) {
        mediaPlayerClientRefs.put(id, WeakReference(client))
    }
}

internal fun removeMediaPlayerClient(id: Int) {
    synchronized(mediaPlayerClientRefs) {
        mediaPlayerClientRefs.remove(id)
    }
}

internal fun scanBlockAndExit() {
    synchronized(mediaPlayerClientRefs) {
        var blockedCount = 0
        mediaPlayerClientRefs.forEach { _, value ->
            value.get().let {
                if (it is KtMediaPlayerClient && it.isClientBlocked) {
                    blockedCount++
                }
            }
        }
        if (blockedCount > 0) {
            if (blockedCount >= 3 || blockedCount == mediaPlayerClientRefs.size()) {
                try {
                    exitProcess(0)
                } catch (ignore: Exception) {
                }
            }
        }
    }
}


internal class PlayerDeathBinder(private val id: Int, service: KtMediaService)
    : IBinder.DeathRecipient {
    private val serviceRef = WeakReference(service)
    private fun getService(): KtMediaService? {
        return serviceRef.get()
    }

    override fun binderDied() {
        getService()?.apply {
            synchronized(mediaPlayerClientRefs) {
                getMediaPlayerClient(id)?.let {
                    if (it is KtMediaPlayerClient) {
                        it.detachDeath()
                        it.actionClientDeath()
                    }
                }
            }
        }
        removeMediaPlayerClient(id)
    }
}


fun startMediaService(context: Context, loader: ILibraryLoader?) {
    synchronized(serviceConnection) {
        if (isServiceConnected) {
            return
        }

        val intent = Intent(context, KtMediaService::class.java)
        loader?.let {
            // TODO
        }

        try {
            if (context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)) {
                // todo
                // do some log
            }
        } catch (ignore: Exception) {
        }
    }
}


fun stopMediaService(context: Context) {
    synchronized(serviceConnection) {
        val intent = Intent(context, KtMediaService::class.java)
        try {
            context.unbindService(serviceConnection)
            context.stopService(intent)
        } catch (ignore: Exception) {
            serviceConnection.onServiceDisconnected(null)
        }
    }
}

