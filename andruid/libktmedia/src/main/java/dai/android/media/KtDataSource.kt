package dai.android.media

import android.os.Parcel
import android.os.Parcelable


class KtSourceItem(val quality: Int, val url: String) : Parcelable {

    constructor() : this(0, "")


    override fun writeToParcel(parcel: Parcel, flags: Int) {
    }

    override fun describeContents(): Int {
        return 0
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    constructor(parcel: Parcel) : this()

    companion object CREATOR : Parcelable.Creator<KtSourceItem> {
        override fun createFromParcel(parcel: Parcel): KtSourceItem {
            return KtSourceItem(parcel)
        }

        override fun newArray(size: Int): Array<KtSourceItem?> {
            return arrayOfNulls(size)
        }
    }

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


class KtDataSource() : Parcelable {


    override fun writeToParcel(parcel: Parcel, flags: Int) {
    }


    override fun describeContents(): Int {
        return 0
    }


    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    constructor(parcel: Parcel) : this()

    companion object CREATOR : Parcelable.Creator<KtDataSource> {
        override fun createFromParcel(parcel: Parcel): KtDataSource {
            return KtDataSource(parcel)
        }

        override fun newArray(size: Int): Array<KtDataSource?> {
            return arrayOfNulls(size)
        }
    }
}
