package dai.android.media.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import dai.android.media.loadLibrariesOnce

class KtMediaService() : Service() {
    private val TAG: String = "KtMediaService"

    private val mStub = KtMediaServiceStub(this)

    override fun onCreate() {
        super.onCreate()
    }

    override fun onUnbind(intent: Intent?): Boolean {
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.d(TAG, "[onBind]")

        val extra = intent?.extras
        try {
            loadLibrariesOnce(extra)
        } catch (ignore: Exception) {
            return null
        }

        return mStub
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "[onStartCommand]")

        val extra = intent?.extras
        try {
            loadLibrariesOnce(extra)
        } catch (ignore: Exception) {
        }

        return START_NOT_STICKY
    }

}
