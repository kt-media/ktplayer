package dai.android.media.service;

import dai.android.media.service.IKtMediaPlayerClient;
import dai.android.media.service.IKtMediaPlayerClientCallback;
import dai.android.media.KtMediaType;

interface IKtMediaPlayerService {
    IKtMediaPlayerClient attachMediaPlayerClient(int id, IKtMediaPlayerClientCallback cb, in KtMediaType mediaType);
    void detachMediaPlayerClient(int id);
}
