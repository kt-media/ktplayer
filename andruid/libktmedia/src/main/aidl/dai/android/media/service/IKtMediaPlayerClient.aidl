
package dai.android.media.service;

import android.view.Surface;
import dai.android.media.KtDataSource;

interface IKtMediaPlayerClient {

    void setDataSourceStr(String path);

    void setDataSourceFd(in ParcelFileDescriptor fd);

    void setDataSourceKt(in KtDataSource ds);

    void prepareAsync();

    void start();

    void pause();

    void stop();

    void release();

    void reset();

    void setSurface(in Surface surface);

    long getCurrentPosition();

    long getDuration();

    void seekTo(long msec, int mode);

    void setVolume(float left, float right);
}
