#! /usr/bin/env bash
set -e

ROOT_DIR=$(pwd)

# this root script compile all android target project
# include ffmpeg openssl srt ...

cd ${ROOT_DIR}/scripts/android
sh compile-all.sh ${ROOT_DIR}
